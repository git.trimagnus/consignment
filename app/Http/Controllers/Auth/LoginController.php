<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;
Use Redirect;
use Cookie;

class LoginController extends Controller
{
    public function login()
    {
        if (session()->has('apitoken')) {
            return redirect('/');
        }
        return view('auth.login');
    }

    public function doLogin(Request $request)
    {
        $request->validate([
            'username' => 'required',
            'password' => 'required'
        ]);

        $username = $request->username;
        
        $client = new \GuzzleHttp\Client();
        $request = $client->post(config('global.url').'/auth/getToken', [
            'json' => $request->all()
        ]);

        $response = json_decode($request->getBody());
        $response_description = $response->header->response_description;

        try {
            $role = $response->body->user->role->name;
        }
        catch (\Exception $e) {
            return redirect('login')->with('status', $response_description);
        }

        if ($role == 'ADMIN' || $role == 'MANAGER') {
            try {
                $token = $response->body->token;
                $user_id = $response->body->user->id;
            }
            catch (\Exception $e) {
                return redirect('login')->with('status', $response_description);
            }
    
            Session::put('apitoken', $token);
            Session::put('username', $username);
            Session::put('user_id', $user_id);
            Session::put('role', $role);

            return redirect('/');
        } else {
            return redirect('login')->with('status', $response_description);
        }
     
    }
    
    public function doLogout()
    {
        Session::flush();
        return redirect('login');
    }
    
}
