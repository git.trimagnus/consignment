<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class EventController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $token = session('apitoken');   
        $client = new \GuzzleHttp\Client();

        $headers = [
            'Authorization' => 'Bearer ' . $token,
            'Content-Type' => 'application/json'
        ];
        
        $request = $client->post(config('global.url').'/event', [
            'headers' => $headers,
            'body'    => json_encode([
                'header' => [
                    'page' => 1,
                    'sort' => 'asc'
                ],
                'body' => [
                    
                ]
            ])
        ]);

        $response = json_decode($request->getBody());
        $data = $response->body;
        
        return Datatables::of($data)->make(true);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $promo = $request->promo;
        /*split the string bases on the @ position*/
        $parts = explode('@', $promo);
        $no_invoice = $request->no_invoice;
        $sku = $parts[0];
        $discount = $parts[1];

        $token = session('apitoken');   
        $client = new \GuzzleHttp\Client();

        $headers = [
                'Authorization' => 'Bearer ' . $token,
                'Content-Type' => 'application/json'
                ];
        

            $request = $client->post(config('global.url').'/sales/updateEvent', [
                'headers' => $headers,
                'body'    => json_encode([
                        'header' => [
                            'page' => 1,
                            'sort' => 'asc'
                        ],
                        'body' => [
                            'no_invoice' => $no_invoice,
                            'discount' => $discount,
                            'sku_id' => $sku
                        ]
                    ])
                ]);

        $data = json_decode($request->getBody());

        if ($data->header->response_code != 99) {
            return response()->json($data->header, 404);
        }

        return response()->json($data->header, 200);

        

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getBrand($group_ds)
    {
        $token = session('apitoken');   
        $client = new \GuzzleHttp\Client();

        $headers = [
            'Authorization' => 'Bearer ' . $token,
            'Content-Type' => 'application/json'
        ];
        
        $request = $client->post(config('global.url').'/event', [
            'headers' => $headers,
            'body'    => json_encode([
                'header' => [
                    'page' => 1,
                    'sort' => 'asc'
                ],
                'body' => [
                    'group_ds' => $group_ds
                ]
            ])
        ]);

        $response = json_decode($request->getBody());
        $data = $response->body;
        
        return Datatables::of($data)->make(true);
    }

    public function getPromo($group_ds, $brand)
    {
        $token = session('apitoken');   
        $client = new \GuzzleHttp\Client();

        $headers = [
            'Authorization' => 'Bearer ' . $token,
            'Content-Type' => 'application/json'
        ];
        
        $request = $client->post(config('global.url').'/event', [
            'headers' => $headers,
            'body'    => json_encode([
                'header' => [
                    'page' => 1,
                    'sort' => 'asc'
                ],
                'body' => [
                    'group_ds' => $group_ds,
                    'brand' => $brand
                ]
            ])
        ]);

        $response = json_decode($request->getBody());
        $data = $response->body;
        
        return Datatables::of($data)->make(true);
    }

    public function getEvent()
    {
        return view('event');
    }
}