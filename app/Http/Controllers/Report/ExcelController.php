<?php

namespace App\Http\Controllers\Report;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Excel;

class ExcelController extends Controller
{
    public function UserReport()
    {
        $token = session('apitoken');   
        $client = new \GuzzleHttp\Client();

        $headers = [
            'Authorization' => 'Bearer ' . $token,
            'Content-Type' => 'application/json'
        ];
        
        $request = $client->post(config('global.url').'/user/report', [
            'headers' => $headers
        ]);

        $response = json_decode($request->getBody());
        $data = $response->body;


        Excel::create('User Report', function($excel) use ($data) {
            $excel->sheet('Excel sheet', function($sheet) use ($data) {
                $sheet->loadView('report.excel.report-user')->with('data',$data);
            });
    
        })->export('xlsx');

    }

    public function UserOutletReport(){
        $token = session('apitoken');   
        $client = new \GuzzleHttp\Client();

        $headers = [
            'Authorization' => 'Bearer ' . $token,
            'Content-Type' => 'application/json'
        ];
        
        $request = $client->post(config('global.url').'/warehouseUser/report', [
            'headers' => $headers
        ]);

        $response = json_decode($request->getBody());
        $data = $response->body;


        Excel::create('User Outlet Report', function($excel) use ($data) {
            $excel->sheet('Excel sheet', function($sheet) use ($data) {
                $sheet->loadView('report.excel.report-user-outlet')->with('data',$data);
            });
    
        })->export('xlsx');

    }

    public function SalesReport(Request $request)
    {
        $from = date('Y-m-d  H:i:s', strtotime($request->from));
        $to = date('Y-m-d  23:59:59', strtotime($request->to));
        switch ($request->status) { 
            case null: 
                $is_approved = null;
            break;
            
            case '': 
                $is_approved = null;
            break; 
        
            case 'Open': 
                $is_approved = '0';
            break;
        
            case 'Approved': 
                $is_approved = '1';
            break; 

            case 'Rejected': 
                $is_approved = '2';
            break; 
        }

        $token = session('apitoken');   
        $client = new \GuzzleHttp\Client();

        $headers = [
            'Authorization' => 'Bearer ' . $token,
            'Content-Type' => 'application/json'
        ];
        
        $request = $client->post(config('global.url').'/sales/report', [
            'headers' => $headers,
            'json'    => [
                'from' => $from,
                'to'   => $to,
                'is_approved' => $is_approved
            ]
        ]);

        $response = json_decode($request->getBody());
        $data = $response->body;


        Excel::create('Sales Report', function($excel) use ($data) {
            $excel->sheet('Excel sheet', function($sheet) use ($data) {
                $sheet->loadView('report.excel.report-sales')->with('data',$data);
            });
    
        })->export('xlsx');

    }

    public function SalesReportOutlet(Request $request)
    {
        $from = date('Y-m-d 00:00:00', strtotime($request->from));
        $to = date('Y-m-d 23:59:59', strtotime($request->to));
        $cust_code = $request->cust_code;
        $user_id = $request->user_id;
 

        $token = session('apitoken');   
        $client = new \GuzzleHttp\Client();

        $headers = [
            'Authorization' => 'Bearer ' . $token,
            'Content-Type' => 'application/json'
        ];
        
        $request = $client->post(config('global.url').'/sales/reportoutlet', [
            'headers' => $headers,
            'json'    => [
                'from' => $from,
                'to'   => $to,
                'cust_code' => $cust_code,
                'user_id' => $user_id
            ]
        ]);

        $response = json_decode($request->getBody());
        $data = $response->body;


        Excel::create('Transaction Recap Outlet', function($excel) use ($data) {
            $excel->sheet('Excel sheet', function($sheet) use ($data) {
                $sheet->loadView('report.excel.report-sales-outlet')->with([
                    'data' => $data
                    ]);
            });
    
        })->export('xlsx');

    }
}
