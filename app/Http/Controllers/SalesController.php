<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class SalesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $token = session('apitoken');   
        $client = new \GuzzleHttp\Client();

        $headers = [
            'Authorization' => 'Bearer ' . $token,
            'Content-Type' => 'application/json'
        ];
        
        $request = $client->post(config('global.url').'/sales/read', [
            'headers' => $headers,
            'body'    => json_encode([
                'header' => [
                    'page' => 1,
                    'sort' => 'desc'
                ],
                'body' => [
                    
                ]
            ])
        ]);

        $response = json_decode($request->getBody());
        $data = $response->body;
        
        return Datatables::of($data)
        ->editColumn('is_approved', function ($data){
            if ($data->is_approved == 0) {
                return 'Open';
            } elseif ($data->is_approved == 1) {
                return 'Approved';      
            } elseif ($data->is_approved == 2) {
                return 'Rejected';
            }
        })
        ->editColumn('tanggal', function ($data) {
            return date('d F Y', strtotime($data->tanggal));
        })
        ->addColumn('outlet', function ($data) {
            return '<p>'.$data->kd_counter. " - " .$data->keterangan.'</p>';
        })
        ->addColumn('action', function ($data) {
            return '<a onclick="detail(' . $data->id . ')" class="btn btn-secondary">Detail</a>';
        })
        ->rawColumns(['outlet', 'action'])
        ->make(true);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $token = session('apitoken');   
        $client = new \GuzzleHttp\Client();

        $headers = [
            'Authorization' => 'Bearer ' . $token,
            'Content-Type' => 'application/json'
        ];
        
        $request = $client->post(config('global.url').'/sales', [
            'headers' => $headers,
            'json'    => [
                'header' => [
                    'id' => 0,
                    'page' => 1,
                    'sort' => 'desc'
                ],
                'body' => [
                    'id' => $id
                ]
            ]
        ]);

        $response = json_decode($request->getBody());
        $data = $response->body;

        return Datatables::of($data)
        ->editColumn('is_approved', function ($data){
            if ($data->is_approved == 0) {
                return 'Open';
            } elseif ($data->is_approved == 1) {
                return 'Approved';      
            } elseif ($data->is_approved == 2) {
                return 'Rejected';
            }
        })
        ->editColumn('tanggal', function ($data) {
            return date('d F Y', strtotime($data->tanggal));
        })
        ->addColumn('action', function ($data) {
            return '<a onclick="details(' . $data->id . ')" class="btn btn-secondary">Details</a>';
        })
        ->make(true);
    }

    public function read(Request $request)
    {
       
        $start_date = date('Y-m-d', strtotime($request->start_date));
        $end_date = date('Y-m-d', strtotime($request->end_date));
        $kd_counter = ($request->kd_counter == '' || $request->kd_counter == null) ? '' : $request->kd_counter;
        switch ($request->is_approved) { 
            case null: 
                $is_approved = '';
            break;
            
            case '': 
                $is_approved = '';
            break; 
        
            case 'Open': 
                $is_approved = '0';
            break;
        
            case 'Approved': 
                $is_approved = '1';
            break; 

            case 'Rejected': 
                $is_approved = '2';
            break; 
        }

        $token = session('apitoken');   
        $client = new \GuzzleHttp\Client();

        $headers = [
            'Authorization' => 'Bearer ' . $token,
            'Content-Type' => 'application/json'
        ];
        
        $request = $client->post(config('global.url').'/sales/read', [
            'headers' => $headers,
            'json'    => [
                'header' => [
                    'id' => 0,
                ],
                'body' => [
                    'start_date' => $start_date,
                    'end_date' => $end_date,
                    'kd_counter' => $kd_counter,
                    'is_approved' => $is_approved,
                ]
            ]
        ]);

        $response = json_decode($request->getBody());
        $data = $response->body;
        
        return Datatables::of($data)
        ->editColumn('is_approved', function ($data){
            if ($data->is_approved == 0) {
                return 'Open';
            } elseif ($data->is_approved == 1) {
                return 'Approved';      
            } elseif ($data->is_approved == 2) {
                return 'Rejected';
            }
        })
        ->editColumn('tanggal', function ($data) {
            return date('d F Y', strtotime($data->tanggal));
        })
        ->addColumn('outlet', function ($data) {
            return '<p>'.$data->kd_counter. " - " .$data->keterangan.'</p>';
        })
        ->addColumn('action', function ($data) {
            return '<a onclick="detail(' . $data->id . ')" class="btn btn-secondary">Detail</a>';
        })
        ->rawColumns(['outlet', 'action'])
        ->make(true);
    }

    public function detail($id)
    {
        $token = session('apitoken');   
        $client = new \GuzzleHttp\Client();

        $headers = [
            'Authorization' => 'Bearer ' . $token,
            'Content-Type' => 'application/json'
        ];
        
        $request = $client->post(config('global.url').'/sales/detail', [
            'headers' => $headers,
            'json'    => [
                'header' => [
                    'id' => 0,
                    'page' => 1,
                    'sort' => 'desc'
                ],
                'body' => [
                    'id' => $id
                ]
            ]
        ]);

        $response = json_decode($request->getBody());
        $data = [];
        array_push($data,$response->body);

        return Datatables::of($data)
        ->editColumn('is_approved', function ($data){
            if ($data->is_approved == 0) {
                return 'Open';
            } elseif ($data->is_approved == 1) {
                return 'Approved';      
            } elseif ($data->is_approved == 2) {
                return 'Rejected';
            }
        })
        ->editColumn('tanggal', function ($data) {
            return date('d F Y', strtotime($data->tanggal));
        })
        ->addColumn('action', function ($data) {
            return '<a onclick="details(' . $data->id . ')" class="btn btn-secondary">Details</a>';
        })
        ->make(true);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getSales()
    {
        return view('sales');
    }

    public function salesApproved(Request $request)
    {
        $start_date = date('Y-m-d', strtotime($request->start_date));
        $end_date = date('Y-m-d', strtotime($request->end_date));
        $kd_counter = ($request->kd_counter == '' || $request->kd_counter == null) ? '' : $request->kd_counter;
        switch ($request->is_approved) { 
            case null: 
                $is_approved = null;
            break;
            
            case '': 
                $is_approved = null;
            break; 
        
            case 'Open': 
                $is_approved = '0';
            break;
        
            case 'Approved': 
                $is_approved = '1';
            break; 

            case 'Rejected': 
                $is_approved = '2';
            break; 
        }
        
        $token = session('apitoken');   
        $client = new \GuzzleHttp\Client();

        $headers = [
            'Authorization' => 'Bearer ' . $token,
            'Content-Type' => 'application/json'
        ];
        
        $request = $client->post(config('global.url').'/sales/read', [
            'headers' => $headers,
            'json'    => [
                'header' => [
                    'id' => 0,
                ],
                'body' => [
                    'start_date' => $start_date,
                    'end_date' => $end_date,
                    'kd_counter' => $kd_counter,
                    'is_approved' => $is_approved,
                ]
            ]
        ]);

        $response = json_decode($request->getBody());
        $data = $response->body;
        
        return Datatables::of($data)
        ->editColumn('is_approved', function ($data){
            if ($data->is_approved == 0) {
                return 'Open';
            } elseif ($data->is_approved == 1) {
                return 'Approved';      
            } elseif ($data->is_approved == 2) {
                return 'Rejected';
            }
        })
        ->editColumn('tanggal', function ($data) {
            return date('d F Y', strtotime($data->tanggal));
        })
        ->addColumn('outlet', function ($data) {
            return '<p>'.$data->kd_counter. " - " .$data->keterangan.'</p>';
        })
        ->addColumn('action', function ($data) {
            return '<a onclick="edit(' . $data->id . ')" class="btn btn-secondary">Edit</a>';
        })
        ->rawColumns(['outlet', 'action'])
        ->make(true);
    }

    public function salesOutlet(Request $request)
    {
        $from = date('Y-m-d 00:00:00', strtotime($request->from));
        $to = date('Y-m-d 23:59:59', strtotime($request->to));
        $cust_code = $request->cust_code;
        $user_id = $request->user_id;

        $token = session('apitoken');   
        $client = new \GuzzleHttp\Client();

        $headers = [
            'Authorization' => 'Bearer ' . $token,
            'Content-Type' => 'application/json'
        ];
        
        $request = $client->post(config('global.url').'/sales/reportoutlet', [
            'headers' => $headers,
            'json'    => [
                'from' => $from,
                'to'   => $to,
                'cust_code' => $cust_code,
                'user_id'   => $user_id
            ]
        ]);

        $response = json_decode($request->getBody());
        $data = $response->body;

        return Datatables::of($data)->make(true);
    }

    public function getEditSku()
    {
        return view('edit-sku');
    }
}
