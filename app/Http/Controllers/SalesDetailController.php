<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class SalesDetailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $start_date = date('Y-m-d', strtotime($request->start_date));
        $end_date = date('Y-m-d', strtotime($request->end_date));
        $kd_counter = $request->kd_counter;

        $token = session('apitoken');   
        $client = new \GuzzleHttp\Client();

        $headers = [
            'Authorization' => 'Bearer ' . $token,
            'Content-Type' => 'application/json'
        ];
        
        $request = $client->post(config('global.url').'/SalesDetailItem', [
            'headers' => $headers,
            'body'    => json_encode([
                'header' => [
                    'page' => 1,
                    'sort' => 'desc'
                ],
                'body' => [
                    'start_date' => $start_date,
                    'end_date' => $end_date,
                    'kd_counter' => $kd_counter,
                    'is_approved' => 1
                ]
            ])
        ]);

        $response = json_decode($request->getBody());
        $data = $response->body;
        
        return Datatables::of($data)
        ->editColumn('is_approved', function ($data){
            if ($data->is_approved == 0) {
                return 'Open';
            } elseif ($data->is_approved == 1) {
                return 'Approved';      
            } elseif ($data->is_approved == 2) {
                return 'Rejected';
            }
        })
        ->editColumn('tanggal', function ($data) {
            return date('d F Y', strtotime($data->tanggal));
        })
        ->addColumn('outlet', function ($data) {
            return '<p>'.$data->kd_counter. " - " .$data->keterangan.'</p>';
        })
        ->addColumn('action', function ($data) {
            return '<a onclick="edit(' . $data->id . ')" class="btn btn-secondary">Edit</a>';
        })
        ->rawColumns(['outlet', 'action'])
        ->make(true);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $token = session('apitoken');   
        $client = new \GuzzleHttp\Client();

        $headers = [
            'Authorization' => 'Bearer ' . $token,
            'Content-Type' => 'application/json'
        ];
        
        $request = $client->post(config('global.url').'/SalesDetailItem/show', [
            'headers' => $headers,
            'body'    => json_encode([
                'header' => [
                    'page' => 1,
                    'sort' => 'desc'
                ],
                'body' => [
                    'is_approved' => 1,
                    'id' => $id
                ]
            ])
        ]);

        $response = json_decode($request->getBody());
        $data = $response->body;

        return Datatables::of($data)
        ->editColumn('is_approved', function ($data){
            if ($data->is_approved == 0) {
                return 'Open';
            } elseif ($data->is_approved == 1) {
                return 'Approved';      
            } elseif ($data->is_approved == 2) {
                return 'Rejected';
            }
        })
        ->editColumn('tanggal', function ($data) {
            return date('d F Y', strtotime($data->tanggal));
        })
        ->addColumn('outlet', function ($data) {
            $outlet = $data->kd_counter. " - " .$data->keterangan;
            return $outlet;
        })
        ->addColumn('action', function ($data) {
            return '<a onclick="edit(' . $data->id . ')" class="btn btn-secondary">Edit</a>';
        })
        ->make(true);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'tanggal' => 'required',
            'no_invoice' => 'required',
            'outlet'  => 'required',
            'article'    => 'required',
            'stock'   => 'required',
            'price'   => 'required'
        ]);

        // return $request->all();
 
        $token = session('apitoken');   
        $client = new \GuzzleHttp\Client();

        $headers = [
            'Authorization' => 'Bearer ' . $token,
            'Content-Type' => 'application/json'
            ];

        $sendrequest = ['header' => [
                            'id' => 0
                                ],  
                            'body' => [
                                'filters' => [
                                    'id' => $id
                                ],
                                'attributes' => [
                                    'price' => $request->price,
                                    'stock' => $request->stock,
                                    'address'  => $request->address,
                                    'phone'    => $request->phone,
                                    'status'   => $request->status
                                ]  
                            ]        
                        ];

        $request = $client->post(config('global.url').'/SalesDetailItem/update', [
                    'headers' => $headers,
                    'json'    => $sendrequest
                ]);

        $data = json_decode($request->getBody());
   
        if ($data->header->response_code != 99) {
            return response()->json($data->header, 409);
        }

        return response()->json($data->header, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getSalesDetail()
    {
        return view('sales-detail');
    }
}
