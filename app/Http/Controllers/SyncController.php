<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SyncController extends Controller
{
    public function sync_outlet()
    {
        $token = session('apitoken');   
        $client = new \GuzzleHttp\Client();

        $headers = [
            'Authorization' => 'Bearer ' . $token,
            'Content-Type' => 'application/json'
        ];

        $sendrequest = ['header' => [
                            'id'   => 0,
                            'size' => 10,
                            'page' => 1,
                            'sort' => 'asc'
                        ],
                        'body' => [
                            
                        ],
                    ];
        
        $request = $client->post(config('global.url').'/sync/warehouse', [
            'headers' => $headers,
            'json'    => $sendrequest
        ]);

        $response = json_decode($request->getBody());
        $response_description = $response->header->response_description;

        return response()->json($response_description, 200);
           
    }

    public function sync_article()
    {
        $master_item = DB::connection('sqlsrv_inventory_sales')->select('SELECT
            KdArtSize AS kd_art_size,
            Kdartikel AS kd_article,
            SKUID as sku_id,
            Size as size,
            Nama as nama,
            Unit as unit,
            Price as price,
            Jenis as jenis,
            Discount as discount,
            Companycode as company_code,
            userid as user_id,
            dateinput as date_input,
            userupdate as user_update,
            dateupdate as date_update,
            Linkgambar as link_gambar,
            Priceputus as price_putus,
            HPP as hpp
        FROM
            MasterItem');

        $array = json_decode(json_encode($master_item), true);

        $insert_data = collect($array); // Make a collection to use the chunk method

        // it will chunk the dataset in smaller collections containing 500 values each. 
        // Play with the value to get best result
        $chunks = $insert_data->chunk(500);

        DB::table('mst_item')->delete();

        foreach ($chunks as $chunk)
        {
            DB::table('mst_item')->insert($chunk->toArray());
        }

        return response()->json([
            'message' => 'success'
        ], 200);

    }

    public function sync_event()
    {
        $master_sku = DB::connection('sqlsrv_post_erandra')->select('SELECT
            SKU AS sku,
            Brand AS brand,
            Promo AS promo,
            Disc AS disc,
            Margin AS margin,
            SharingESM AS sharing_esm,
            SharingDS AS sharing_ds,
            GroupDS AS group_ds,
            PeriodeAwal AS periode_awal,
            PeriodeAkhir AS periode_akhir,
            Dateinput AS date_input,
            Userinput AS user_input,
            Dateupdate AS date_update,
            UserUpdate AS user_update,
            FreeItem AS free_item,
            SpecialPrice AS special_price 
        FROM
            MasterSKU');

        $array = json_decode(json_encode($master_sku), true);

        $insert_data = collect($array); // Make a collection to use the chunk method

        // it will chunk the dataset in smaller collections containing 500 values each. 
        // Play with the value to get best result
        $chunks = $insert_data->chunk(500);

        DB::table('mst_sku')->delete();

        foreach ($chunks as $chunk)
        {
            DB::table('mst_sku')->insert($chunk->toArray());
        }

        return response()->json([
            'message' => 'success'
        ], 200);
    }
        
}
