<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class UserOutletController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $token = session('apitoken');   
        $client = new \GuzzleHttp\Client();

        $headers = [
            'Authorization' => 'Bearer ' . $token,
            'Content-Type' => 'application/json'
        ];
         
        $request = $client->post(config('global.url').'/warehouseUser',[
            'headers' => $headers,
            'body'    => json_encode([
                'header' => [
                    'page' => 1,
                    'sort' => 'asc'
                ],
                'body' => [

                ]
            ])
        ]);

        $response = json_decode($request->getBody());
        $data = $response->body;

        return Datatables::of($data)
        ->editColumn('gudang', function($data){
            $gudang = '';
            foreach ($data->gudang as $item) {
               $gudang .= '<p>'.$item->kd_gdg_art. " - " .$item->keterangan.'</p>';  
            }
            return $gudang;
        })
        ->addColumn('action', function($data){
            return '<a onclick="editData('. $data->id .')" class="btn btn-primary btn-action mr-1" data-toggle="tooltip" title="Edit Data" data-original-title="Edit"><i class="fas fa-edit"></i></a>';
        })
        ->rawColumns(['gudang', 'action'])
        ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user_id            = $request->user_id;
        $warehouse_checkbox = $request->outlet_checkbox;
        $count              = collect($request->outlet_checkbox)->count();

        if ($count == 0) {
            return response()->json([
                'message' => 'please checked before submit'
            ], 404);
        }

        $sendrequest = ['header' => [
                            'id' => 0
                            ],
                        'body' => [
                            'user_id' => $user_id,
                            'gudang_lokasi' => $warehouse_checkbox
                            ]        
                    ];

            $token = session('apitoken');   
            $client = new \GuzzleHttp\Client();

            $headers = [
                'Authorization' => 'Bearer ' . $token,
                'Content-Type' => 'application/json'
                ];

            $request = $client->post(config('global.url').'/warehouseUser/create', [
                'headers' => $headers,
                'json'    => $sendrequest
                ]);
  

        $data = json_decode($request->getBody());

        if ($data->header->response_code == 21) {
            return response()->json($data->header, 404);
        }

        return response()->json($data->header, 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $token = session('apitoken');   
        $client = new \GuzzleHttp\Client();

        $headers = [
            'Authorization' => 'Bearer ' . $token,
            'Content-Type' => 'application/json'
        ];
         
        $request = $client->post(config('global.url').'/warehouseUser',[
            'headers' => $headers,
            'body'    => json_encode([
                'header' => [
                    'page' => 1,
                    'sort' => 'asc'
                ],
                'body' => [
                    'id' => $id
                ]
            ])
        ]);

        $response = json_decode($request->getBody());
        $data = $response->body;

        return response()->json($data[0], 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $user_id            = $request->user_id_edit;
        $warehouse_checkbox = $request->outlet_checkbox;

        $token = session('apitoken');   
        $client = new \GuzzleHttp\Client();

        $headers = [
                'Authorization' => 'Bearer ' . $token,
                'Content-Type' => 'application/json'
            ];
        
            $request = $client->post(config('global.url').'/warehouseUser/delete',[
                'headers' => $headers,
                'body'    => json_encode([
                    'body' => [
                        'user_id'    => $user_id
                    ]
                ])
            ]);

        $sendrequest = ['header' => [
                            'id' => 0
                            ],
                        'body' => [
                            'user_id' => $user_id,
                            'gudang_lokasi' => $warehouse_checkbox
                            ]        
                    ];

        $request = $client->post(config('global.url').'/warehouseUser/create', [
            'headers' => $headers,
            'json'    => $sendrequest
            ]);

        return response()->json([
            'message' => 'Successful Updated'
        ], 200);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user_id = $id;
        
        $token = session('apitoken');   
        $client = new \GuzzleHttp\Client();

        $headers = [
            'Authorization' => 'Bearer ' . $token,
            'Content-Type' => 'application/json'
        ];
         
        $request = $client->post(config('global.url').'/warehouseUser/delete',[
            'headers' => $headers,
            'body'    => json_encode([
                'body' => [
                    'user_id'    => $user_id
                ]
            ])
        ]);

        $data = json_decode($request->getBody());

        return response()->json($data->header, 200);
        
    }
    
    public function getUserOutlet()
    {
        return view('user-outlet');
    }

    public function listUser()
    {
        $token = session('apitoken');   
        $client = new \GuzzleHttp\Client();

        $headers = [
            'Authorization' => 'Bearer ' . $token,
            'Content-Type' => 'application/json'
        ];
         
        $request = $client->post(config('global.url').'/warehouseUser/show',[
            'headers' => $headers,
            'body'    => json_encode([
                'header' => [
                    'page' => 1,
                    'sort' => 'asc'
                ],
                'body' => [
                    
                ]
            ])
        ]);

        $response = json_decode($request->getBody());
        $data = $response->body;

        return Datatables::of($data)->make(true);
    }

}
