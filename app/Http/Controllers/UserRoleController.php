<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class UserRoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $token = session('apitoken');   
        $client = new \GuzzleHttp\Client();

        $headers = [
            'Authorization' => 'Bearer ' . $token,
            'Content-Type' => 'application/json'
        ];
         
        $request = $client->post(config('global.url').'/userRole',[
            'headers' => $headers,
            'body'    => json_encode([
                'header' => [
                    'page' => 1,
                    'sort' => 'asc'
                ],
                'body' => [
                    
                ]
            ])
        ]);

        $response = json_decode($request->getBody());
        $data = $response->body;

        return Datatables::of($data)
        ->addColumn('action', function($data){
            return '<a onclick="deleteData('. $data->id .')" class="btn btn-danger btn-action mr-1" data-toggle="tooltip" title="" data-original-title="Delete"><i class="fas fa-trash"></i></a>';
        })->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'user_id' => 'required|integer',
            'role_id' => 'required|integer'
        ]);

        $token = session('apitoken');   
        $client = new \GuzzleHttp\Client();

        $headers = [
            'Authorization' => 'Bearer ' . $token,
            'Content-Type' => 'application/json'
        ];

   
        $sendrequest = [ 'header' => [
                            'id' => 0
                            ],
                        'body' => [
                            'user_id' => $request->user_id,
                            'role_id' => $request->role_id
                            ]        
                        ];
          
        $request = $client->post(config('global.url').'/userRole/add', [
            'headers' => $headers,
            'json'    => $sendrequest
        ]); 
    
    
        $data = json_decode($request->getBody());

        if ($data->header->response_code == 21) {
            return response()->json($data->header, 409);
        }

        return response()->json($data->header, 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $token = session('apitoken');   
        $client = new \GuzzleHttp\Client();

        $headers = [
        'Authorization' => 'Bearer ' . $token,
        'Content-Type' => 'application/json'
        ];

        $sendrequest = [ 'body' => [
            'id' => $id
            ]        
        ];

        $request = $client->post(config('global.url').'/userRole', [
            'headers' => $headers,
            'json'    => $sendrequest
        ]);

        $data = json_decode($request->getBody());

        if ($data->header->response_code == 21) {
            return response()->json($data->header, 409);
        }

        return response()->json($data->body[0], 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $token = session('apitoken');   
        $client = new \GuzzleHttp\Client();

        $headers = [
            'Authorization' => 'Bearer ' . $token,
            'Content-Type' => 'application/json'
        ];

        // request to get user_id
        $request = $client->post(config('global.url').'/userRole',[
            'headers' => $headers,
            'body'    => json_encode([
                'header' => [
                    'id' => 0
                ],
                'body' => [
                    'id' => $id
                ]
            ])
        ]); 

        $data = json_decode($request->getBody());
        $user_id = $data->body[0]->user_id;

        $request = $client->post(config('global.url').'/warehouseUser/delete',[
            'headers' => $headers,
            'body'    => json_encode([
                'header' => [
                    'id' => 0
                ],
                'body' => [
                    'user_id' => $user_id
                ]
            ])
        ]); 

        $request = $client->post(config('global.url').'/userRole/delete',[
            'headers' => $headers,
            'body'    => json_encode([
                'header' => [
                    'id' => 0
                ],
                'body' => [
                    'id' => $id
                ]
            ])
        ]);

        $data = json_decode($request->getBody());

        return response()->json($data->header, 200);
    }

    public function getUserRole()
    {
        return view('user-role');
    }

    public function listUser()
    {
        $token = session('apitoken');   
        $client = new \GuzzleHttp\Client();

        $headers = [
            'Authorization' => 'Bearer ' . $token,
            'Content-Type' => 'application/json'
        ];
         
        $request = $client->post(config('global.url').'/userRole/show',[
            'headers' => $headers,
            'body'    => json_encode([
                'header' => [
                    'page' => 1,
                    'sort' => 'asc'
                ],
                'body' => [
                    
                ]
            ])
        ]);

        $response = json_decode($request->getBody());
        $data = $response->body;

        return Datatables::of($data)->make(true);
    }

}
