@extends('layouts.master')

@section('content')
<div class="main-sidebar">
    <aside id="sidebar-wrapper">
        <div class="sidebar-brand">
        <a href="{{ url('/') }}">Consignment</a>
        </div>
        <div class="sidebar-brand sidebar-brand-sm">
        <a href="{{ url('/') }}">Cons</a>
        </div>
        <ul class="sidebar-menu">
        <li class="menu-header">Main Navigation</li>
        <li class="dropdown">
            <a href="{{ url('/') }}" class="nav-link"><i class="fas fa-fire"></i><span>Dashboard</span></a>
        </li>
        @if(Session::get('role') == 'ADMIN')
        <li class="dropdown">
            <a href="#" class="nav-link has-dropdown" data-toggle="dropdown"><i class="far fa-user"></i> <span>User Management</span></a>
            <ul class="dropdown-menu">
              <li><a class="nav-link" href="{{ url('/user') }}"></i>User</a></li>
              <li><a class="nav-link" href="{{ url('/user-role') }}"></i>User Rules</a></li>
              <li><a class="nav-link" href="{{ url('/user-outlet') }}"></i>Mapping User - Outlet</a></li>
            </ul>
        </li>
        <li class="dropdown">
            <a href="{{ url('/article') }}" class="nav-link"><i class="fas fa-table"></i><span>Article</span></a>
        </li>
        <li class="dropdown">
            <a href="{{ url('/outlet') }}" class="nav-link"><i class="fas fa-table"></i><span>Outlet</span></a>
        </li>
        <li class="dropdown">
            <a href="{{ url('/event') }}" class="nav-link"><i class="fas fa-table"></i><span>Event</span></a>
        </li>
        @endif
        <li class="dropdown active">
            <a href="#" class="nav-link has-dropdown" data-toggle="dropdown"><i class="fas fa-table"></i> <span>Sales</span></a>
            <ul class="dropdown-menu">
            <li><a class="nav-link" href="{{ url('/sales') }}"></i>List Sales</a></li>
            @if(Session::get('role') == 'ADMIN')
            <li class="active"><a class="nav-link" href="{{ url('/edit-sku') }}"></i>Edit Sku Event</a></li>
            <li><a class="nav-link" href="{{ url('/sales-detail') }}"></i>Sales Detail</a></li>
            @endif
            </ul>
          </li>
          <li class="dropdown">
            <a href="#" class="nav-link has-dropdown" data-toggle="dropdown"><i class="fas fa-table"></i> <span>Report</span></a>
            <ul class="dropdown-menu">
            <li><a class="nav-link" href="{{ url('report/outlet') }}"></i>
                Transaction Recap Outlet</a></li>
            </ul>
          </li>
          {{-- <li class="dropdown">
            <a href="#" class="nav-link has-dropdown" data-toggle="dropdown"><i class="fas fa-table"></i> <span>Stock Moving</span></a>
            <ul class="dropdown-menu">
            <li><a class="nav-link" href="{{ url('/item-transfer') }}"></i>Item Transfer</a></li>
            </ul>
          </li> --}}
    </aside>
</div>
      
<!-- Main Content -->
<div class="main-content">
    <section class="section">
      <div class="section-header">
        <h1>Edit Sku Event</h1>
        <div class="section-header-breadcrumb">
          <div class="breadcrumb-item active"><a href="{{ url('/') }}">Home</a></div>
          <div class="breadcrumb-item">Sales</div>
          <div class="breadcrumb-item">Edit Sku Event</div>
        </div>
      </div>

      <div class="section-body">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                  {{-- <div class="container-fluid">
                      <a onclick="refresh()" class="btn btn-outline-warning float-right">
                        <i class="fas fa-sync-alt"></i>  Refresh</a>   
                  </div> --}}
              </div>
              <div class="card-body">
                    <label for="datepicker"><b>Filter :</b></label>
                    <div class="row">
                      <div class="col-md-3 col-sm">
                        <input type="text" class="form-control" name="min" id="min" placeholder="Select Minimum Date">
                      </div>
                      <div class="col-md-3 col-sm">
                          <input type="text" class="form-control" name="max" id="max" placeholder="Select Maximum Date">
                        </div>
                        <div class="col-md-3 col-sm">
                          <select class="form-control select2 result" style="width:100%" id="kd_counter"
                              name="kd_counter" required>

                          </select>
                        </div>
                        <div class="col-md-2 col-sm"> 
                          <a onclick="refresh()" class="btn btn-outline-warning">
                                  <i class="fas fa-sync-alt"></i> Refresh</a>
                      </div>
                      {{-- <div class="col-md-2 col-sm">
                          <select class="form-control" name="status" id="status">
                            <option value="">All</option>
                            <option>Open</option>
                            <option>Approved</option>
                            <option>Rejected</option>
                          </select>
                        </div>
                        <div class="col-md-2 col-sm">
                            <a href="#" id="export_excel" class="btn btn-info">Export Excel <i class="far fa-file-excel"></i></a>
                        </div> --}}
                    </div>
                    <br>                
                    <div class="table-responsive">
                        <table class="table table-striped" id="sales-datatable">
                          <thead>                                 
                            <tr>
                              <th>Tanggal</th>
                              <th>No. Invoice</th>
                              <th>Outlet</th>
                              <th>Status</th>
                              <th>Action</th>
                            </tr>
                          </thead>
                          <tbody>                                 
                           
                          </tbody>
                        </table>
                      </div>     
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>


{{-- Modal--}}
<div class="modal fade" id="modal-form" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog" role="document">
      <div class="modal-content">
          <div class="modal-header">
              <h5 class="modal-title"></h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
              </button>
          </div>
          <div class="modal-body">
              <form method="post">
                  @csrf
                  @method('POST')
                  <input type="hidden" id="id" name="id">
                  <div class="form-group">
                      <label for="tanggal" class="col-form-label">Tanggal</label>
                      <input class="form-control" id="tanggal" name="tanggal" required readonly>                 
                  </div>

                  <div class="form-group">
                      <label for="no_invoice" class="col-form-label">No. Invoice</label>
                      <input class="form-control" id="no_invoice" name="no_invoice" required readonly>                         
                  </div>

                  <div class="form-group">
                      <label for="outlet" class="col-form-label">Outlet</label>
                      <select class="form-control" style="width:100%;" id="outlet" name="outlet" required>
                      </select>                 
                  </div>

                  <div class="form-group">
                    <label for="brand" class="col-form-label">Brand</label>
                    <select class="form-control" style="width:100%;" id="brand" name="brand" required>
                    </select> 
                </div>

                  <div class="form-group">
                      <label for="promo" class="col-form-label">Promo</label>
                      <select class="form-control" style="width:100%;" id="promo" name="promo" required>
                      </select>                     
                  </div>

                  {{-- <div class="form-group">
                      <label for="price" class="col-form-label">Price</label>
                      <input type="number" class="form-control" id="price" name="price" required>                    
                  </div> --}}

                  <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                      <button type="submit" class="btn btn-primary">Submit</button>
                  </div>
              </form>
          </div>
      </div>
  </div>
</div>
{{-- /Modal --}}

@endsection

@section('javascript')

<script type="text/javascript">
var table = $('#sales-datatable').DataTable();

        //export excel
          $('#export_excel').click(function(){        
            var from = $('#min').val();
            var to = $('#max').val();
            var status = $('#status').val();
            window.open("{{url('/')}}"+"/api/sales/report/xls?from="+from+"&to="+to+"&status="+status);
        });

        $(function(){

          $.ajax({
            url: "{{ url('api/outlet') }}",
            type: "GET",
            dataType: "JSON",
            success: function (response) {
                $('#kd_counter').empty();
                $('#kd_counter').append('<option value="" selected>All</option>');
                $.each(response.data, function (key, val) {
                    $('#kd_counter').append('<option value="' + val.kd_gdg_art + '">' + val
                        .kd_gdg_art + ' - ' + val.keterangan +  '</option>');
                });
            }
        });

          var start_date = $('#min').val('{{date("m/d/Y")}}');
          var end_date = $('#max').val('{{date("m/d/Y")}}');
    
          var min = $('#min').datepicker();
          var max = $('#max').datepicker();
          var url = "{{url('/api/sales-approved')}}";
          $.ajax({
            url: url,
            type: "GET",
            data:{
                'start_date' : '{{date("m/d/Y")}}',
                'end_date' : '{{date("m/d/Y")}}',
                'kd_counter' : '',
                'is_approved' : 'Approved',
            },
            success: function (response) {
                table.clear().draw();
                $.each(response.data, function (key, val) {
                    table.row.add([
                        val.tanggal, val.no_invoice, val.outlet,
                        val.is_approved, val.action
                    ]).draw();
                });
                swal.close();
            },
            error : function(response){
              swal.close();

                swal({
                    title: 'Opps...',
                    text: data.responseJSON.response_description,
                    type: 'error',
                    timer: '2000'
                })
            }
        });
      });


      function refresh() {
        var start_date = $('#min').val();
        var end_date = $('#max').val();
        var kd_counter = $('#kd_counter').val();
        var is_approved = $('#status').val();
        var url = "{{url('/api/sales-approved')}}";
        $.ajax({
            url: url,
            type: "GET",
            data:{
                'start_date' : start_date,
                'end_date' : end_date,
                'kd_counter' : kd_counter,
                'is_approved' : "Approved",
            },
            beforeSend: function() {
                swal({
                    title: 'Now loading',
                    allowEscapeKey: false,
                    allowOutsideClick: false,
                    onOpen: () => {
                    swal.showLoading();
                    }
                })
            },
            success: function (response) {
                table.clear().draw();
                $.each(response.data, function (key, val) {
                    table.row.add([
                        val.tanggal, val.no_invoice, val.outlet,
                        val.is_approved, val.action
                    ]).draw();
                });
                swal.close();
            },
            error : function(response){
              swal.close();

                swal({
                    title: 'Opps...',
                    text: data.responseJSON.response_description,
                    type: 'error',
                    timer: '2000'
                })
            }
        });
      }

      $(document).ready(function() {
        $('#outlet').select2();
        $('#brand').select2();
        $('#promo').select2();

        $.ajax({
            url: "{{ url('api/user-outlet') }}" + '/' + {{ Session::get('user_id')}},
            type: "GET",
            dataType: "JSON",
            success: function(response){
              // console.log(response.gudang);
                $('#outlet').empty();
                $('#outlet').append('<option disabled selected>-</option>');
                $.each(response.gudang, function(key, val){
                    $('#outlet').append('<option value="'+ val.cust_code +'">'+ val.kd_gdg_art + ' - ' + val.keterangan + '</option>');
                });
            }
            });

          $('#outlet').change(function(){
              group_ds = $('#outlet').val();

                $.ajax({
                    url: "{{ url('api/event/brand') }}" + '/' + group_ds,
                    type: "GET",
                    dataType: "JSON",
                    success: function(response){
                      // console.log(response.data);
                      $('#brand').empty();
                      $('#brand').append('<option disabled selected>-</option>');
                      $.each(response.data, function(key, val){
                          $('#brand').append('<option value="'+ val.brand +'">' + val.brand + '</option>');
                      });
                      
                    }
                });
            });

            $('#outlet, #brand').change(function(){
              group_ds = $('#outlet').val();
              brand = $('#brand').val();

                $.ajax({
                    url: "{{ url('api/event/brand') }}" + '/' + group_ds + '/' + brand,
                    type: "GET",
                    dataType: "JSON",
                    success: function(response){
                      console.log(response);
                      $('#promo').empty();
                      $('#promo').append('<option disabled selected>-</option>');
                      $.each(response.data, function(key, val){
                          $('#promo').append('<option value="'+ val.sku + '@' + val.disc +'">' + val.promo + '</option>');
                      });
                      
                    }
                });
            });
        });

      

      //function convert number
      function number_format(number, decimals, decPoint, thousandsSep){
          decimals = decimals || 0;
          number = parseFloat(number);

          if(!decPoint || !thousandsSep){
              decPoint = '.';
              thousandsSep = ',';
          }

          var roundedNumber = Math.round( Math.abs( number ) * ('1e' + decimals) ) + '';
          var numbersString = decimals ? roundedNumber.slice(0, decimals * -1) : roundedNumber;
          var decimalsString = decimals ? roundedNumber.slice(decimals * -1) : '';
          var formattedNumber = "";

          while(numbersString.length > 3){
              formattedNumber += thousandsSep + numbersString.slice(-3)
              numbersString = numbersString.slice(0,-3);
          }

          return (number < 0 ? '-' : '') + numbersString + formattedNumber + (decimalsString ? (decPoint + decimalsString) : '');
      }

      function edit(id) {
        save_method = 'edit';
        $('input[name=_method]').val('PATCH');
        $('#modal-form form')[0].reset();
        $.ajax({
          url: "{{ url('api/salesdetail') }}" + '/' + id,
          type: "GET",
          dataType: "JSON",
          success: function(response) {  

            $('#modal-form').modal('show');
            $('.modal-title').text('Edit Event');

            $('#id').val(response.data[0].id);
            $('#tanggal').val(response.data[0].tanggal);
            $('#no_invoice').val(response.data[0].no_invoice);
          },
          error : function() {
              alert("Nothing Data");
          }
        });
      }

      $(function(){
        $('#modal-form form').on('submit', function (e) {
            if (!e.isDefaultPrevented()){
                var id = $('#id').val();
                url = "{{ url('api/event') . '/' }}" + id;
                $.ajax({
                    url : url,
                    type : "POST",
                    data : $('#modal-form form').serialize(),
                    success : function(data) {             
                        refresh();
                        swal({
                            title: 'Success!',
                            text: data.message,
                            type: 'success',
                            timer: '1500'
                        })
                      $('#modal-form').modal('hide');
                    },
                    error : function(data){
                        swal({
                            title: 'Opps...',
                            text: data.responseJSON.response_description,
                            type: 'error',
                            timer: '2000'
                        })
                    }
                });
                return false;
            }
        });
    });

  </script>
    
@endsection