@extends('layouts.master')

@section('content')
<div class="main-sidebar">
    <aside id="sidebar-wrapper">
        <div class="sidebar-brand">
        <a href="{{ url('/') }}">Consignment</a>
        </div>
        <div class="sidebar-brand sidebar-brand-sm">
        <a href="{{ url('/') }}">Cons</a>
        </div>
        <ul class="sidebar-menu">
        <li class="menu-header">Main Navigation</li>
        <li class="dropdown">
            <a href="{{ url('/') }}" class="nav-link"><i class="fas fa-fire"></i><span>Dashboard</span></a>
        </li>
        @if(Session::get('role') == 'ADMIN')
        <li class="dropdown">
            <a href="#" class="nav-link has-dropdown" data-toggle="dropdown"><i class="far fa-user"></i> <span>User Management</span></a>
            <ul class="dropdown-menu">
                <li><a class="nav-link" href="{{ url('/user') }}"></i>User</a></li>
                <li><a class="nav-link" href="{{ url('/user-role') }}"></i>User Rules</a></li>
                <li><a class="nav-link" href="{{ url('/user-outlet') }}"></i>Mapping User - Outlet</a></li>
            </ul>
        </li>
        <li class="dropdown">
            <a href="{{ url('/article') }}" class="nav-link"><i class="fas fa-table"></i><span>Article</span></a>
        </li>
        <li class="dropdown">
            <a href="{{ url('/outlet') }}" class="nav-link"><i class="fas fa-table"></i><span>Outlet</span></a>
        </li>
        <li class="dropdown active">
            <a href="{{ url('/event') }}" class="nav-link"><i class="fas fa-table"></i><span>Event</span></a>
        </li>
        @endif
        <li class="dropdown">
            <a href="#" class="nav-link has-dropdown" data-toggle="dropdown"><i class="fas fa-table"></i> <span>Sales</span></a>
            <ul class="dropdown-menu">
            <li><a class="nav-link" href="{{ url('/sales') }}"></i>List Sales</a></li>
            @if(Session::get('role') == 'ADMIN')
            <li><a class="nav-link" href="{{ url('/edit-sku') }}"></i>Edit Sku Event</a></li>
            <li><a class="nav-link" href="{{ url('/sales-detail') }}"></i>Sales Detail</a></li>
            @endif
            </ul>
        </li>
        <li class="dropdown">
          <a href="#" class="nav-link has-dropdown" data-toggle="dropdown"><i class="fas fa-table"></i> <span>Report</span></a>
          <ul class="dropdown-menu">
          <li><a class="nav-link" href="{{ url('report/outlet') }}"></i>
              Transaction Recap Outlet</a></li>
          </ul>
        </li>
        {{-- <li class="dropdown">
          <a href="#" class="nav-link has-dropdown" data-toggle="dropdown"><i class="fas fa-table"></i> <span>Stock Moving</span></a>
          <ul class="dropdown-menu">
          <li><a class="nav-link" href="{{ url('/item-transfer') }}"></i>Item Transfer</a></li>
          </ul>
        </li> --}}
    </aside>
</div>
      
<!-- Main Content -->
<div class="main-content">
    <section class="section">
      <div class="section-header">
        <h1>Event</h1>
        <div class="section-header-breadcrumb">
          <div class="breadcrumb-item active"><a href="{{ url('/') }}">Home</a></div>
          <div class="breadcrumb-item">Event</div>
        </div>
      </div>

      <div class="section-body">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                  <div class="container-fluid">
                      <a id="sync" class="btn btn-outline-warning float-right">
                          <i class="fas fa-sync-alt"></i>  Synchronize</a>
                      <a onclick="refresh()" class="btn btn-outline-warning float-right">
                        <i class="fas fa-sync-alt"></i>  Refresh</a>   
                  </div>
              </div>
              <div class="card-body">
                <div class="table-responsive">
                  <table class="table table-striped" id="event-datatable">
                    <thead>                                 
                      <tr>
                          <th>Kode Sku</th>
                          <th>Brand</th>
                          <th>Promo</th>
                          <th>Discount</th>
                          <th>Margin</th>
                          <th>Sharing Erandra</th>
                          <th>Sharing Dept Store</th>
                          <th>Group Dept Store</th>
                      </tr>
                    </thead>
                    <tbody>                                 
                     
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
@endsection

@section('javascript')

<script type="text/javascript">
var table = $('#event-datatable').DataTable({
                      processing: true,
                      // serverSide: true,
                      ajax: "{{ url('api/event') }}",
                      columns: [
                        {data: 'sku', name: 'sku'},
                        {data: 'brand', name: 'brand'},
                        {data: 'promo', name: 'promo'},
                        {data: 'disc', name: 'disc'},
                        {data: 'margin', name: 'margin'},
                        {data: 'sharing_esm', name: 'sharing_esm'},
                        {data: 'sharing_ds', name: 'sharing_ds'},
                        {data: 'group_ds', name: 'group_ds'}
                      ]
                    });

      function refresh() {
          table.ajax.reload();
      }

      $(function () {
        $('#sync').on('click', function () {
            swal({
              title: 'Apakah kamu yakin?',
              text: "Jangan melakukan Synchronize ketika server10 tidak aktif atau data akan terhapus semua. Membutuhkan 10 - 20 detik untuk melakukan Synchronize.",
              type: 'warning',
              showCancelButton: true,
              cancelButtonColor: '#d33',
              confirmButtonColor: '#3085d6',
              confirmButtonText: 'Yes, Synchronize it!'
          }).then(function () {
            swal({
                title: 'Now loading',
                allowEscapeKey: false,
                allowOutsideClick: false,
                timer: 30000,
                onOpen: () => {
                swal.showLoading();
                }
            })
            $.ajax({
                url : "{{ url('api/sync-event') }}",
                  type : "GET",
                  success : function(data) {
                      table.ajax.reload();
                      swal({
                          title: 'Synchronize Success!',
                          type: 'success',
                          timer: '1500'
                      })
                  },
                  error : function () {
                      swal({
                          title: 'Oops...',
                          type: 'error',
                          timer: '1500'
                      })
                  }
              });
          });
        });
    });

  </script>
    
@endsection