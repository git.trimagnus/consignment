<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Consignment</title>

  @include('common.styles')

</head>
<body>

  <div id="app">
    <div class="wrapper">
      @include('components.header')
        @yield('content')
      @include('components.footer')
    </div>
  </div>

  @include('common.scripts')
  @yield('javascript')

</body>
</html>