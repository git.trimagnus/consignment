<html>
    <head>
        <style>
            table { border-collapse: collapse; border: 1px solid black; }
        </style>
    </head>
    <body>
        <table width="100%">
            <tr>
                <td>-</td>
                <td>Tanggal</td>
                <td>Group Outlet</td>
                <td>Kode Outlet</td>
                <td>Outlet</td>
                <td>Jml. Sale Consignment</td>
                <td>Jml. No Sale</td>
            </tr>
            @foreach ($data as $row)
            <tr>
                <td>-</td>
                <td>{{$row->tanggal}}</td>
                <td>{{$row->cust_code}}</td>
                <td>{{$row->kd_counter}}</td>
                <td>{{$row->keterangan}}</td>
                <td>{{$row->jumlah_sc}}</td>
                <td>{{$row->jumlah_ns}}</td>
            </tr>
            @endforeach   
        </table> 
    </body>
</html>