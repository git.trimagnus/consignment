<html>
    <head>
        <style>
            table { border-collapse: collapse; border: 1px solid black; }
        </style>
    </head>
    <body>
            <tr>
                <td>Tanggal</td>
                <td>No. Invoice</td>
                <td>Outlet</td>
                <td>Qty</td>
                <td>Bruto</td>
                <td>Netto</td>
                <td>Status</td>
            </tr>
            @foreach ($data as $row)
            <tr>
                <td>{{date('d F Y', strtotime($row->tanggal))}}</td>
                <td>{{$row->no_invoice}}</td>
                <td>{{$row->kd_counter. ' - ' .$row->keterangan}}</td>
                <td>{{$row->qty}}</td>
                <td>{{$row->bruto}}</td>
                <td>{{$row->netto}}</td>
                <td>
                    @if ($row->is_approved == '0')
                        {{'Open'}} 
                    @elseif($row->is_approved == '1')
                        {{'Approved'}}
                    @else
                        {{'Rejected'}}
                    @endif
                </td>
            </tr>
            @endforeach   
        </table> 
    </body>
</html>