<html>
    <head>
        <style>
            table { border-collapse: collapse; border: 1px solid black; }
        </style>
    </head>
    <body>
        <table width="100%">
            <tr>
                <td>UserID</td>
                <td>Username</td>
                <td>Fullname</td>
                <td>Address</td>
                <td>Phone</td>
                <td>Role</td>
            </tr>
            @foreach ($data as $row)
            <tr>
                <td>{{$row->user_id}}</td>
                <td>{{$row->username}}</td>
                <td>{{$row->fullname}}</td>
                <td>{{$row->address}}</td>
                <td>{{$row->phone}}</td>
                <td>{{$row->role}}</td>
            </tr>
            @endforeach   
        </table> 
    </body>
</html>