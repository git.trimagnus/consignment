@extends('layouts.master')

@section('content')
<div class="main-sidebar">
    <aside id="sidebar-wrapper">
        <div class="sidebar-brand">
            <a href="{{ url('/') }}">Consignment</a>
        </div>
        <div class="sidebar-brand sidebar-brand-sm">
            <a href="{{ url('/') }}">Cons</a>
        </div>
        <ul class="sidebar-menu">
            <li class="menu-header">Main Navigation</li>
            <li class="dropdown">
                <a href="{{ url('/') }}" class="nav-link"><i class="fas fa-fire"></i><span>Dashboard</span></a>
            </li>
            @if(Session::get('role') == 'ADMIN')
            <li class="dropdown">
                <a href="#" class="nav-link has-dropdown" data-toggle="dropdown"><i class="far fa-user"></i> <span>User
                        Management</span></a>
                <ul class="dropdown-menu">
                    <li><a class="nav-link" href="{{ url('/user') }}"></i>User</a></li>
                    <li><a class="nav-link" href="{{ url('/user-role') }}"></i>User Rules</a></li>
                    <li><a class="nav-link" href="{{ url('/user-outlet') }}"></i>Mapping User - Outlet</a></li>
                </ul>
            </li>
            <li class="dropdown">
                <a href="{{ url('/article') }}" class="nav-link"><i class="fas fa-table"></i><span>Article</span></a>
            </li>
            <li class="dropdown">
                <a href="{{ url('/outlet') }}" class="nav-link"><i class="fas fa-table"></i><span>Outlet</span></a>
            </li>
            <li class="dropdown">
                <a href="{{ url('/event') }}" class="nav-link"><i class="fas fa-table"></i><span>Event</span></a>
            </li>
            @endif
            <li class="dropdown">
                <a href="#" class="nav-link has-dropdown" data-toggle="dropdown"><i class="fas fa-table"></i>
                    <span>Sales</span></a>
                <ul class="dropdown-menu">
                    <li><a class="nav-link" href="{{ url('/sales') }}"></i>List Sales</a></li>
                    @if(Session::get('role') == 'ADMIN')
                    <li><a class="nav-link" href="{{ url('/edit-sku') }}"></i>Edit Sku Event</a></li>
                    <li><a class="nav-link" href="{{ url('/sales-detail') }}"></i>Sales Detail</a></li>
                    @endif
                </ul>
            </li>
            <li class="dropdown active">
                <a href="#" class="nav-link has-dropdown" data-toggle="dropdown"><i class="fas fa-table"></i>
                    <span>Report</span></a>
                <ul class="dropdown-menu">
                    <li class="active"><a class="nav-link" href="{{ url('report/outlet') }}"></i>
                            Transaction Recap Outlet</a></li>
                </ul>
            </li>
            {{-- <li class="dropdown">
          <a href="#" class="nav-link has-dropdown" data-toggle="dropdown"><i class="fas fa-table"></i> <span>Stock Moving</span></a>
          <ul class="dropdown-menu">
          <li><a class="nav-link" href="{{ url('/item-transfer') }}"></i>Item Transfer</a></li>
        </ul>
        </li> --}}
    </aside>
</div>

<!-- Main Content -->
<div class="main-content">
    <section class="section">
        <div class="section-header">
            <h1>Transaction Recap Outlet</h1>
            <div class="section-header-breadcrumb">
                <div class="breadcrumb-item active"><a href="{{ url('/') }}">Home</a></div>
                <div class="breadcrumb-item">Sales</div>
                <div class="breadcrumb-item">Transaction Recap Outlet</div>
            </div>
        </div>

        <div class="section-body">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="container-fluid">
                      <a onclick="refresh()" class="btn btn-outline-warning float-right">
                        <i class="fas fa-sync-alt"></i>  Refresh</a>   
                  </div>
                        </div>
                        <div class="card-body">
                            <label for="datepicker"><b>Filter :</b></label>
                            <div class="row">
                                <div class="col-md-4 col-sm">
                                    <input type="text" class="form-control result" name="min" id="min"
                                        placeholder="Select Minimum Date">
                                </div>
                                <div class="col-md-4 col-sm">
                                    <input type="text" class="form-control result" name="max" id="max"
                                        placeholder="Select Maximum Date">
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-md-4 col-sm">
                                <label for="datepicker"><b>Group Outlet :</b></label>
                                    <select class="form-control select2 result" style="width:100%" id="cust_code"
                                        name="cust_code" required>

                                    </select>
                                </div>
                                <div class="col-md-4 col-sm">
                                    <label for="datepicker"><b>User :</b></label>
                                    <select class="form-control select2 result" style="width:100%" id="user_id"
                                        name="user_id" required>

                                    </select>
                                </div>
                                <div class="col-md-2 col-sm">
                                    <a href="#" id="export_excel" class="btn btn-info">Export Excel <i
                                            class="far fa-file-excel"></i></a>
                                </div>
                            </div>
                            <br>
                            <div class="table-responsive">
                                <table class="table table-striped" id="report-sales-outlet-datatable">
                                    <thead>
                                        <tr>
                                            <th>Tanggal</th>
                                            <th>Group Outlet</th>
                                            <th>Kode Outlet</th>
                                            <th>Outlet</th>
                                            <th>Jml. Sale Consignment</th>
                                            <th>Jml. No Sale</th>
                                        </tr>
                                    </thead>
                                    <tbody id="report-sales-outlet-body">

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

@endsection

@section('javascript')

<script type="text/javascript">
    var table = $('#report-sales-outlet-datatable').DataTable({
        processing: true,
                   
    });

    function refresh(){
        var from = $('#min').val();
        var to = $('#max').val();
        var cust_code = $('#cust_code option:selected').val();
        var user_id = $('#user_id option:selected').val();

        var url = "{{url('/')}}" + "/api/report-outlet?from=" + from + "&to=" + to + "&cust_code=" +
            cust_code + "&user_id=" + user_id;

        $.ajax({
            url: url,
            type: "GET",
            dataType: "JSON",
            beforeSend: function() {
                swal({
                    title: 'Now loading',
                    allowEscapeKey: false,
                    allowOutsideClick: false,
                    timer: 30000,
                    onOpen: () => {
                    swal.showLoading();
                    }
                })
            },
            success: function (response) {
                table.clear().draw();
                $.each(response.data, function (key, val) {
                    table.row.add([
                        val.tanggal, val.cust_code, val.kd_counter, 
                        val.keterangan, val.jumlah_sc, val.jumlah_ns
                    ]).draw();
                });
                swal({
                    title: 'Refresh Success!',
                    type: 'success',
                    timer: '500'
                })
            }
        });
    }

    //export excel
    $('#export_excel').click(function () {
        var from = $('#min').val();
        var to = $('#max').val();
        var cust_code = $('#cust_code option:selected').val();
        var user_id = $('#user_id option:selected').val();
        window.open("{{url('/')}}" + "/api/sales/report-outlet/xls?from=" + from + "&to=" + to + "&cust_code=" +
            cust_code + "&user_id=" + user_id);
    });

    $(function () {
        $("#min").datepicker({});
        $("#max").datepicker({});
        $.ajax({
            url: "{{ url('api/user') }}",
            type: "GET",
            dataType: "JSON",
            success: function (response) {
                $('#user_id').empty();
                $('#user_id').append('<option value="" selected>All</option>');
                $.each(response.data, function (key, val) {
                    $('#user_id').append('<option value="' + val.id + '">' +  '[ ' + val.username + ' ]  ' + val.fullname + '</option>');
                });
            }
        });

        $.ajax({
            url: "{{ url('api/warehouse/group') }}",
            type: "GET",
            dataType: "JSON",
            success: function (response) {
                $('#cust_code').empty();
                $('#cust_code').append('<option value="" selected>All</option>');
                $.each(response.data, function (key, val) {
                    $('#cust_code').append('<option value="' + val.cust_code + '">' + val
                        .cust_code + '</option>');
                });
            }
        });

    });

    $(function () {
        $(".result").change(function () {
            var html = "";
            var from = $('#min').val();
            var to = $('#max').val();
            var cust_code = $('#cust_code option:selected').val();
            var user_id = $('#user_id option:selected').val();

            var url = "{{url('/')}}" + "/api/report-outlet?from=" + from + "&to=" + to + "&cust_code=" +
                cust_code + "&user_id=" + user_id;

            $.ajax({
                url: url,
                type: "GET",
                dataType: "JSON",
                success: function (response) {
                    table.clear().draw();

                    $.each(response.data, function (key, val) {
                        table.row.add([
                            val.tanggal, val.cust_code, val.kd_counter, 
                            val.keterangan, val.jumlah_sc, val.jumlah_ns
                        ]).draw();
                    });
                }
            });
        });
    });

</script>

@endsection
