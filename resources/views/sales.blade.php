@extends('layouts.master')

@section('content')
<div class="main-sidebar">
    <aside id="sidebar-wrapper">
        <div class="sidebar-brand">
        <a href="{{ url('/') }}">Consignment</a>
        </div>
        <div class="sidebar-brand sidebar-brand-sm">
        <a href="{{ url('/') }}">Cons</a>
        </div>
        <ul class="sidebar-menu">
        <li class="menu-header">Main Navigation</li>
        <li class="dropdown">
            <a href="{{ url('/') }}" class="nav-link"><i class="fas fa-fire"></i><span>Dashboard</span></a>
        </li>
        @if(Session::get('role') == 'ADMIN')
        <li class="dropdown">
            <a href="#" class="nav-link has-dropdown" data-toggle="dropdown"><i class="far fa-user"></i> <span>User Management</span></a>
            <ul class="dropdown-menu">
              <li><a class="nav-link" href="{{ url('/user') }}"></i>User</a></li>
              <li><a class="nav-link" href="{{ url('/user-role') }}"></i>User Rules</a></li>
              <li><a class="nav-link" href="{{ url('/user-outlet') }}"></i>Mapping User - Outlet</a></li>
            </ul>
        </li>
        <li class="dropdown">
            <a href="{{ url('/article') }}" class="nav-link"><i class="fas fa-table"></i><span>Article</span></a>
        </li>
        <li class="dropdown">
            <a href="{{ url('/outlet') }}" class="nav-link"><i class="fas fa-table"></i><span>Outlet</span></a>
        </li>
        <li class="dropdown">
            <a href="{{ url('/event') }}" class="nav-link"><i class="fas fa-table"></i><span>Event</span></a>
        </li>
        @endif
        <li class="dropdown active">
            <a href="#" class="nav-link has-dropdown" data-toggle="dropdown"><i class="fas fa-table"></i> <span>Sales</span></a>
            <ul class="dropdown-menu">
            <li class="active"><a class="nav-link" href="{{ url('/sales') }}"></i>List Sales</a></li>
            @if(Session::get('role') == 'ADMIN')
            <li><a class="nav-link" href="{{ url('/edit-sku') }}"></i>Edit Sku Event</a></li>
            <li><a class="nav-link" href="{{ url('/sales-detail') }}"></i>Sales Detail</a></li>
            @endif
            </ul>
          </li>
        <li class="dropdown">
          <a href="#" class="nav-link has-dropdown" data-toggle="dropdown"><i class="fas fa-table"></i> <span>Report</span></a>
          <ul class="dropdown-menu">
          <li><a class="nav-link" href="{{ url('report/outlet') }}"></i>
              Transaction Recap Outlet</a></li>
          </ul>
        </li>
        {{-- <li class="dropdown">
          <a href="#" class="nav-link has-dropdown" data-toggle="dropdown"><i class="fas fa-table"></i> <span>Stock Moving</span></a>
          <ul class="dropdown-menu">
          <li><a class="nav-link" href="{{ url('/item-transfer') }}"></i>Item Transfer</a></li>
          </ul>
        </li> --}}
    </aside>
</div>
      
<!-- Main Content -->
<div class="main-content">
    <section class="section">
      <div class="section-header">
        <h1>List Sales</h1>
        <div class="section-header-breadcrumb">
          <div class="breadcrumb-item active"><a href="{{ url('/') }}">Home</a></div>
          <div class="breadcrumb-item">Sales</div>
          <div class="breadcrumb-item">List Sales</div>
        </div>
      </div>

      <div class="section-body">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                    <a href="#" id="export_excel" class="btn btn-info">Export Excel <i class="far fa-file-excel"></i></a>
  
              </div>
              <div class="card-body">
                    <label for="datepicker"><b>Filter :</b></label>
                    <div class="row">
                      <div class="col-2">
                        <input type="text" class="form-control" name="min" id="min" placeholder="Select Minimum Date">
                      </div>
                      <div class="col-2">
                          <input type="text" class="form-control" name="max" id="max" placeholder="Select Maximum Date">
                        </div>
                      <div class="col-3">
                        <select class="form-control select2 result" style="width:100%" id="kd_counter"
                            name="kd_counter" required>

                        </select>
                      </div>
                      <div class="col-2">
                          <select class="form-control" name="status" id="status">
                            <option value="">All</option>
                            <option value="Open" selected>Open</option>
                            <option value="Approved">Approved</option>
                            <option value="Rejected">Rejected</option>
                          </select>
                        </div>
                        <div class="col-2"> 
                            <a onclick="refresh()" class="btn btn-outline-warning">
                                    <i class="fas fa-sync-alt"></i> Refresh</a>
                        </div>
                    </div>
                    <br>                
                    <div class="table-responsive">
                        <table class="table table-striped" id="sales-datatable">
                          <thead>                                 
                            <tr>
                              <th>Tanggal</th>
                              <th>No. Invoice</th>
                              <th>Outlet</th>
                              <th>Status</th>
                              <th>No Bon Manual</th>
                              <th>Action</th>
                            </tr>
                          </thead>
                          <tbody>                                 
                           
                          </tbody>
                        </table>
                      </div>     
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>


{{-- Modal --}}
<div class="modal fade" id="modal-form" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
          <div class="modal-header">
              <h5 class="modal-title"></h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
              </button>
          </div> 
           <div class="modal-body">
              <div class="container-fluid">
                  <div class="row">
                      <div class="col-md-6">
                          <div class="form-group">
                              <label for="statustransaction"><b>Status</b></label>
                                <input type="text" class="form-control" id="statustransaction" readonly>                      
                          </div>
                      </div>
                      <div class="col-md-6">
                          <div class="form-group">
                              <label for="tanggal"><b>Tanggal</b></label>
                                <input type="text" class="form-control" id="tanggal" readonly>                      
                          </div>
                      </div>
                      <div class="col-md-6">
                          <div class="form-group">
                              <label for="kd_counter"><b>Outlet</b></label>
                                <input type="text" class="form-control" id="outlet" readonly>                        
                          </div>
                      </div>
                      <div class="col-md-6">
                          <div class="form-group">
                              <label for="sku"><b>No. Sku</b></label>
                                <input type="text" class="form-control" id="sku" readonly>                      
                          </div>
                      </div>
                  </div>
                  <div class="row">
                      <div class="col-md-6">
                          <div class="form-group">
                              <label for="no_invoice"><b>No. Invoice</b></label>
                                <input type="text" class="form-control" id="no_invoice" readonly>                       
                          </div>
                      </div>
                      <div class="col-md-6">
                          <div class="form-group">
                              <label for="no_bon_manual"><b>No. Bon Manual</b></label>
                                <input type="text" class="form-control" id="no_bon_manual" readonly>                     
                          </div>
                      </div>
                  </div>
                  <div class="row">
                    <div class="col-md-12">
                        <div class="table-responsive">
                            <table class="table table-striped" id="detail-datatable">
                              <thead>                                 
                                <tr>
                                  <th>Item</th>
                                  <th>Qty</th>
                                  <th>Harga</th>
                                </tr>
                              </thead>
                              <tbody id="records-details">                                 
                               
                              </tbody>
                              <tfoot id="grand_total">
                     
                              </tfoot>
                            </table>
                          </div>
                    </div>
                  </div>
                </div>
            </div>
                  <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                  </div>
              </form>
          </div>
      </div>
  </div>
</div>
{{-- /Modal --}}

@endsection

@section('javascript')

<script type="text/javascript">
var table = $('#sales-datatable').DataTable();

        //export excel
          $('#export_excel').click(function(){        
            var from = $('#min').val();
            var to = $('#max').val();
            var status = $('#status').val();
            window.open("{{url('/')}}"+"/api/sales/report/xls?from="+from+"&to="+to+"&status="+status);
        });

      $(function(){
        $.ajax({
            url: "{{ url('api/outlet') }}",
            type: "GET",
            dataType: "JSON",
            success: function (response) {
                $('#kd_counter').empty();
                $('#kd_counter').append('<option value="" selected>All</option>');
                $.each(response.data, function (key, val) {
                    $('#kd_counter').append('<option value="' + val.kd_gdg_art + '">' + val
                        .kd_gdg_art + ' - ' + val.keterangan +  '</option>');
                });
            }
        });

          var start_date = $('#min').val('{{date("m/d/Y")}}');
          var end_date = $('#max').val('{{date("m/d/Y")}}');
    
          var min = $('#min').datepicker();
          var max = $('#max').datepicker();
          var url = "{{url('/api/sales-read')}}";
          $.ajax({
            url: url,
            type: "GET",
            data:{
                'start_date' : '{{date("m/d/Y")}}',
                'end_date' : '{{date("m/d/Y")}}',
                'kd_counter' : '',                
                'is_approved' : 'Open',
            },
            success: function (response) {
                table.clear().draw();
                $.each(response.data, function (key, val) {
                  console.log(val)
                    table.row.add([
                        val.tanggal, val.no_invoice, val.outlet,
                        val.is_approved, val.no_bon_manual, val.action
                    ]).draw();
                });
                swal.close();
            },
            error : function(response){
              swal.close();

                swal({
                    title: 'Opps...',
                    text: data.responseJSON.response_description,
                    type: 'error',
                    timer: '2000'
                })
            }
        });
      });


      function refresh() {
        var start_date = $('#min').val();
        var end_date = $('#max').val();
        var kd_counter = $('#kd_counter').val();
        var is_approved = $('#status').val();
        var url = "{{url('/api/sales-read')}}";
        $.ajax({
            url: url,
            type: "GET",
            data:{
                'start_date' : start_date,
                'end_date' : end_date,
                'kd_counter' : kd_counter,  
                'is_approved' : is_approved,
            },
            beforeSend: function() {
                swal({
                    title: 'Now loading',
                    allowEscapeKey: false,
                    allowOutsideClick: false,
                    onOpen: () => {
                    swal.showLoading();
                    }
                })
            },
            success: function (response) {
                table.clear().draw();
                $.each(response.data, function (key, val) {
                    table.row.add([
                        val.tanggal, val.no_invoice, val.outlet,
                        val.is_approved, val.no_bon_manual, val.action
                    ]).draw();
                });
                swal.close();
            },
            error : function(response){
              swal.close();

                swal({
                    title: 'Opps...',
                    text: data.responseJSON.response_description,
                    type: 'error',
                    timer: '2000'
                })
            }
        });
      }

      //function convert number
      function number_format(number, decimals, decPoint, thousandsSep){
          decimals = decimals || 0;
          number = parseFloat(number);

          if(!decPoint || !thousandsSep){
              decPoint = '.';
              thousandsSep = ',';
          }

          var roundedNumber = Math.round( Math.abs( number ) * ('1e' + decimals) ) + '';
          var numbersString = decimals ? roundedNumber.slice(0, decimals * -1) : roundedNumber;
          var decimalsString = decimals ? roundedNumber.slice(decimals * -1) : '';
          var formattedNumber = "";

          while(numbersString.length > 3){
              formattedNumber += thousandsSep + numbersString.slice(-3)
              numbersString = numbersString.slice(0,-3);
          }

          return (number < 0 ? '-' : '') + numbersString + formattedNumber + (decimalsString ? (decPoint + decimalsString) : '');
      }

      function detail(id) {
        $.ajax({
          url: "{{ url('api/salesdetail') }}" + '/' + id,
          type: "GET",
          dataType: "JSON",
          success: function(response) {
            
            $.ajax({
                    url : "{{ url('api/outlet') }}" + '/' + response.data[0].kd_counter,
                    type : "GET",
                    dataType: "JSON",
                    success : function(data) {
                      $('#outlet').val(data.keterangan);

                    },
                    error : function(data){
                      $('#outlet').val(response.data[0].kd_counter);
                    }
                });

            $('#statustransaction').val(response.data[0].is_approved);
            $('#tanggal').val(response.data[0].tanggal);
            $('#no_invoice').val(response.data[0].no_invoice);
            $('#no_bon_manual').val(response.data[0].no_bon_manual);
            $('#sku').val(response.data[0].sku_id);
            
            if (response.data[0].kode_trans == "NS") {
              $('#grand_total').html('');
              var no_sale = '<tr><td colspan="3"><b>'+ response.data[0].remark +'</b></td></tr>';
              $('#records-details').html(no_sale);
            } else {
              var records_details = '';
              $.each(response.data[0].details, function(k, v) {
                records_details += '<tr>' +
                          '<td>' + v.article + '</td>'+
                          '<td>' + v.stock + '</td>'+
                          '<td>' + number_format( v.price, 2, ',', '.' ) + '</td>'+
                        '</tr>';
              });
              $('#records-details').html(records_details);

              var grand_total = '<tr>' +
                                  '<th>Discount :</th>'+
                                  '<th></th>'+
                                  '<th>' + response.data[0].discount + '</th>'+
                                '</tr>' +
                                '<tr>' +
                                  '<th>Net :</th>'+
                                  '<th></th>'+
                                  '<th>' + number_format( response.data[0].total, 2, ',', '.' ) + '</th>'+
                                '</tr>';

              $('#grand_total').html(grand_total);
            }
          },
          error : function() {
              alert("Nothing Data");
          }
        });

        $('#modal-form').modal('show');
        $('.modal-title').text('Transaction Detail');
    }

  </script>
    
@endsection