@extends('layouts.master')

@section('content')
<div class="main-sidebar">
    <aside id="sidebar-wrapper">
        <div class="sidebar-brand">
            <a href="{{ url('/') }}">Consignment</a>
        </div>
        <div class="sidebar-brand sidebar-brand-sm">
            <a href="{{ url('/') }}">Cons</a>
        </div>
        <ul class="sidebar-menu">
            <li class="menu-header">Main Navigation</li>
            <li class="dropdown">
                <a href="{{ url('/') }}" class="nav-link"><i class="fas fa-fire"></i><span>Dashboard</span></a>
            </li>
            @if(Session::get('role') == 'ADMIN')
            <li class="dropdown active">
                <a href="#" class="nav-link has-dropdown" data-toggle="dropdown"><i class="far fa-user"></i> 
                    <span>User Management</span></a>
                <ul class="dropdown-menu">
                    <li><a class="nav-link" href="{{ url('/user') }}"></i>User</a></li>
                    <li><a class="nav-link" href="{{ url('/user-role') }}"></i>User Rules</a></li>
                    <li class="active"><a class="nav-link" href="{{ url('/user-outlet') }}"></i>Mapping User - Outlet</a></li>
                </ul>
            </li>
            <li class="dropdown">
                <a href="{{ url('/article') }}" class="nav-link"><i class="fas fa-table"></i><span>Article</span></a>
            </li>
            <li class="dropdown">
                <a href="{{ url('/outlet') }}" class="nav-link"><i class="fas fa-table"></i><span>Outlet</span></a>
            </li>
            <li class="dropdown">
                <a href="{{ url('/event') }}" class="nav-link"><i class="fas fa-table"></i><span>Event</span></a>
            </li>
            @endif
            <li class="dropdown">
                <a href="#" class="nav-link has-dropdown" data-toggle="dropdown"><i class="fas fa-table"></i> <span>Sales</span></a>
                <ul class="dropdown-menu">
                <li><a class="nav-link" href="{{ url('/sales') }}"></i>List Sales</a></li>
                @if(Session::get('role') == 'ADMIN')
                <li><a class="nav-link" href="{{ url('/edit-sku') }}"></i>Edit Sku Event</a></li>
                <li><a class="nav-link" href="{{ url('/sales-detail') }}"></i>Sales Detail</a></li>
                @endif
                </ul>
            </li>
            <li class="dropdown">
                <a href="#" class="nav-link has-dropdown" data-toggle="dropdown"><i class="fas fa-table"></i> <span>Report</span></a>
                <ul class="dropdown-menu">
                <li><a class="nav-link" href="{{ url('report/outlet') }}"></i>
                    Transaction Recap Outlet</a></li>
                </ul>
              </li>
              {{-- <li class="dropdown">
                <a href="#" class="nav-link has-dropdown" data-toggle="dropdown"><i class="fas fa-table"></i> <span>Stock Moving</span></a>
                <ul class="dropdown-menu">
                <li><a class="nav-link" href="{{ url('/item-transfer') }}"></i>Item Transfer</a></li>
                </ul>
              </li> --}}
    </aside>
</div>

<!-- Main Content -->
<div class="main-content">
    <section class="section">
        <div class="section-header">
            <h1>Mapping User - Outlet</h1>
            <div class="section-header-breadcrumb">
                <div class="breadcrumb-item active"><a href="{{ url('/') }}">Home</a></div>
                <div class="breadcrumb-item">User Management</div>
                <div class="breadcrumb-item">Mapping User - Outlet</div>
            </div>
        </div>

        <div class="section-body">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="col-md-2 col-sm">
                                <a href="#" id="export_excel" class="btn btn-info">Export Excel <i class="far fa-file-excel"></i></a>
                            </div>
                            <div class="container-fluid">
                                <a onclick="addForm()" class="btn btn-outline-primary float-right">
                                    <i class="glyphicon glyphicon-plus"></i> Add</a>
                                <a onclick="refresh()" class="btn btn-outline-warning float-right">
                                    <i class="fas fa-sync-alt"></i> Refresh</a>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-striped" id="user-outlet-datatable">
                                    <thead>
                                        <tr>
                                            <th>Username</th>
                                            <th>Outlet</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>


{{-- Modal Add User Warehouse--}}
<div class="modal fade" id="modal-form" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                <form method="post">
                        @csrf
                        @method('POST')
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="username" class="col-form-label">Username</label>
                                        <select class="form-control" style="width:100%;" id="username" name="user_id" required>
                                        </select>                       
                                </div>
                            </div>
                        </div>
    
                        <div class="row">
                            <div class="col-md-12">
                              <label for="outlet" class="col-form-label">Outlet</label>
                                
                                    <div class="table-responsive">
                                        <table class="table table-striped" id="outlet-add-datatable" style="width:100%;">
                                            <thead>
                                                <tr>
                                                    <th></th>
                                                    <th>Group Outlet</th>
                                                    <th>Kode Outlet</th>
                                                    <th>Alamat</th>
                                                    <th>Outlet</th>
                                                </tr>
                                            </thead>
                                            <tbody>
        
                                            </tbody>
                                        </table>
                                    </div>
                                    

                                    <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button> 
                                            <button type="submit" class="btn btn-primary">Submit</button>
                                        </form>
                                        
                                    </div>
                                
                            </div>
                        </div>  
                    </div>      
                </div>
            </div>
        </div>
    </div>
    {{-- /Modal User Add Warehouse --}}


    {{-- Modal Edit User Warehouse--}}
<div class="modal fade" id="modal-form-edit" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
            <form method="post">
                    @csrf
                    @method('POST')
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="username" class="col-form-label">Username</label>
                                    <input type="text" class="form-control" id="username-edit" readonly>
                                    <input type="hidden" name="user_id_edit" id="user-id-edit">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                          <label for="outlet" class="col-form-label">Outlet</label>
                            
                                <div class="table-responsive">
                                    <table style="width:100%;" class="table table-striped" id="outlet-edit-datatable">
                                        <thead>
                                            <tr>
                                                <th><input type="checkbox" id="selectAll"></th>
                                                <th>Group Outlet</th>
                                                <th>Kode Outlet</th>
                                                <th>Alamat</th>
                                                <th>Outlet</th>
                                            </tr>
                                        </thead>
                                        <tbody>
    
                                        </tbody>
                                    </table>
                                </div>
                                

                                <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button> 
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                    </form>
                                    
                                </div>
                            
                        </div>
                    </div>  
                </div>      
            </div>
        </div>
    </div>
</div>
{{-- /Modal Edit User Warehouse --}}

    


   
@endsection

@section('javascript')

<script type="text/javascript">
var table_user_outlet = $('#user-outlet-datatable').DataTable({
                            processing: true,
                            //   serverSide: true,
                            ajax: "{{ url('api/user-outlet') }}",
                            columns: [
                                {data: 'username', name: 'username'},
                                {data: 'gudang', name: 'gudang'},
                                {data: 'action', name: 'action', orderable: false, searchable: false}
                            ]
                            });

var table_outlet_add = $('#outlet-add-datatable').DataTable({
                            aLengthMenu: [[ -1], [ "All"]],
                            scrollY: 400,
                            scrollX: true,
                            processing: true,
                            //   serverSide: true,
                            ajax: "{{ url('api/outlet') }}",
                            columns: [
                                {data: 'id', name: 'id'},
                                {data: 'cust_code', name: 'cust_code'},
                                {data: 'kd_gdg_art', name: 'kd_gdg_art'},
                                {data: 'alamat', name: 'alamat'},
                                {data: 'keterangan', name: 'keterangan'}
                            ],
                            columnDefs: [
                                    {
                                        targets: 0,
                                        checkboxes: {
                                        selectRow: true
                                        }
                                    }
                                ],
                                select: {
                                    style: 'multi'
                                },
                                order: [[1, 'asc']]
                            });

var table_outlet_edit = $('#outlet-edit-datatable').DataTable({
                        lengthMenu: [ [-1], ["All"] ],
                        scrollY: 400,
                        scrollX: true,
                        //   serverSide: true,
                        ajax: "{{ url('api/outlet') }}",
                        columns: [
                            {data: 'checkbox_edit', name: 'checkbox_edit', orderable: false, searchable: false },
                            {data: 'cust_code', name: 'cust_code'},
                            {data: 'kd_gdg_art', name: 'kd_gdg_art'},
                            {data: 'alamat', name: 'alamat'},
                            {data: 'keterangan', name: 'keterangan'}
                        ]
                        });

    function refresh() {
        table_user_outlet.ajax.reload();
        table_outlet_add.ajax.reload();
        table_outlet_edit.ajax.reload();
    }

    //export excel
    $('#export_excel').click(function(){        
        window.open("{{url('/')}}"+"/api/user-outlet/report/xls");
    });

    function addForm() {
        $('input[name=_method]').val('POST');
        $('#modal-form').modal('show');
        $('#modal-form form')[0].reset();
        $('input[type="hidden"][id="add-checkbox"]').remove();
        table_outlet_add.ajax.reload();
        $.ajax({
            url: "{{ url('api/user-outlet/list/user') }}",
            type: "GET",
            dataType: "JSON",
            success: function(response){
                $('#username').empty();
                $('#username').append('<option disabled selected>-</option>');
                $.each(response.data, function(key, val){
                    $('#username').append('<option value="'+ val.id +'">'+ val.username + '</option>');
                });
            }
            });
        $('.modal-title').text('Input Data');
    }

    $(document).ready(function() {
        $('#username').select2({
            dropdownParent: $("#modal-form")
        });
    });

    function editData(id) {
        $('#user_id').val(id);
        $('input[name=_method]').val('PUT');
        $('#modal-form-edit').modal('show');
        $('#modal-form-edit form')[0].reset();
        $.ajax({
            url: "{{ url('api/user-outlet') }}" + '/' + id,
            type: "GET",
            dataType: "JSON",
            success: function(response){
                $('#username-edit').val(response.username);
                $('#user-id-edit').val(response.id);
                $.each(response.gudang, function(key, val){
                    $("#outlet_checkbox_"+ val.id).prop('checked', true);
                });
            }
            });
        $('.modal-title').text('Edit Mapping User - Outlet');  
    }

    $("#selectAll").click(function () {
        var checkAll = $("#selectAll").prop('checked');
            if (checkAll) {
                $(".outlet_checkbox").prop("checked", true);
            } else {
                $(".outlet_checkbox").prop("checked", false);
            }
        });

    $(".outlet_checkbox").click(function(){
        if($(".outlet_checkbox").length == $(".outlet_checkbox:checked").length) {
            $("#selectAll").prop("checked", true);
        } else {
            $("#selectAll").prop("checked", false);
        }
    });

    function deleteData(id){
        var csrf_token = $('meta[name="csrf-token"]').attr('content');
        swal({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            cancelButtonColor: '#d33',
            confirmButtonColor: '#3085d6',
            confirmButtonText: 'Yes, delete it!'
        }).then(function () {
            $.ajax({
                url : "{{ url('api/user-outlet') }}" + '/' + id,
                type : "POST",
                data : {'_method' : 'DELETE', '_token' : csrf_token},
                success : function(data) {
                    table_user_outlet.ajax.reload();
                    swal({
                        title: 'Success!',
                        text: data.message,
                        type: 'success',
                        timer: '1500'
                    })
                },
                error : function () {
                    swal({
                        title: 'Oops...',
                        text: data.message,
                        type: 'error',
                        timer: '1500'
                    })
                }
            });
        });
        }


    $(function(){
        $('#modal-form form').on('submit', function (e) {
            var rows_selected = table_outlet_add.column(0).checkboxes.selected();
            // Iterate over all selected checkboxes
            $.each(rows_selected, function(index, rowId){
                // Create a hidden element
                $('#modal-form form').append(
                    $('<input>')
                        .attr('type', 'hidden')
                        .attr('id', 'add-checkbox')
                        .attr('name', 'outlet_checkbox[]')
                        .val(rowId)
                );
            });
            if (!e.isDefaultPrevented()){
                $.ajax({
                    url : "{{ url('api/user-outlet') }}",
                    type : "POST",
                    data : $('#modal-form form').serialize(),
                    beforeSend: function() {
                        swal({
                            title: 'Now loading',
                            allowEscapeKey: false,
                            allowOutsideClick: false,
                            timer: 30000,
                            onOpen: () => {
                            swal.showLoading();
                            }
                        })
                    },
                    success : function(data) {
                        $('input[type="hidden"][id="add-checkbox"]').remove();
                        $('#modal-form').modal('hide');
                        table_user_outlet.ajax.reload();
                        swal({
                            title: 'Success!',
                            text: data.message,
                            type: 'success',
                            timer: '1500'
                        })
                    },
                    error : function(data){
                        $('input[type="hidden"][id="add-checkbox"]').remove();
                        swal({
                            title: 'Opps...',
                            text: data.responseJSON.message,
                            type: 'error',
                            timer: '2000'
                        })
                    }
                });
                return false;
            }
        });
    });

    $(function(){
        $('#modal-form-edit form').on('submit', function (e) {
            if (!e.isDefaultPrevented()){
                $.ajax({ 
                    url : "{{ url('api/user-outlet') }}",
                    type : "PUT",
                    data : $('#modal-form-edit form').serialize(),
                    beforeSend: function() {
                        swal({
                            title: 'Now loading',
                            allowEscapeKey: false,
                            allowOutsideClick: false,
                            timer: 30000,
                            onOpen: () => {
                            swal.showLoading();
                            }
                        })
                    },
                    success : function(data) {
                        
                        $('#modal-form-edit').modal('hide');
                        table_user_outlet.ajax.reload();
                        swal({
                            title: 'Success!',
                            text: data.message,
                            type: 'success',
                            timer: '1500'
                        })
                    },
                    error : function(data){
                        swal({
                            title: 'Opps...',
                            text: data.responseJSON.message,
                            type: 'error',
                            timer: '2000'
                        })
                    }
                });
                return false;
            }
        });
    });
</script>
    
@endsection