@extends('layouts.master')

@section('content')
<div class="main-sidebar">
    <aside id="sidebar-wrapper">
        <div class="sidebar-brand">
            <a href="{{ url('/') }}">Consignment</a>
        </div>
        <div class="sidebar-brand sidebar-brand-sm">
            <a href="{{ url('/') }}">Cons</a>
        </div>
        <ul class="sidebar-menu">
            <li class="menu-header">Main Navigation</li>
            <li class="dropdown">
                <a href="{{ url('/') }}" class="nav-link"><i class="fas fa-fire"></i><span>Dashboard</span></a>
            </li>
            @if(Session::get('role') == 'ADMIN')
            <li class="dropdown active">
                <a href="#" class="nav-link has-dropdown" data-toggle="dropdown"><i class="far fa-user"></i> 
                    <span>User Management</span></a>
                <ul class="dropdown-menu">
                    <li><a class="nav-link" href="{{ url('/user') }}"></i>User</a></li>
                    <li class="active"><a class="nav-link" href="{{ url('/user-role') }}"></i>User Rules</a></li>
                    <li><a class="nav-link" href="{{ url('/user-outlet') }}"></i>Mapping User - Outlet</a></li>

                </ul>
            </li>
            <li class="dropdown">
                <a href="{{ url('/article') }}" class="nav-link"><i class="fas fa-table"></i><span>Article</span></a>
            </li>
            <li class="dropdown">
                <a href="{{ url('/outlet') }}" class="nav-link"><i class="fas fa-table"></i><span>Outlet</span></a>
            </li>
            <li class="dropdown">
                <a href="{{ url('/event') }}" class="nav-link"><i class="fas fa-table"></i><span>Event</span></a>
            </li>
            @endif
            <li class="dropdown">
                <a href="#" class="nav-link has-dropdown" data-toggle="dropdown"><i class="fas fa-table"></i> <span>Sales</span></a>
                <ul class="dropdown-menu">
                <li><a class="nav-link" href="{{ url('/sales') }}"></i>List Sales</a></li>
                @if(Session::get('role') == 'ADMIN')
                <li><a class="nav-link" href="{{ url('/edit-sku') }}"></i>Edit Sku Event</a></li>
                <li><a class="nav-link" href="{{ url('/sales-detail') }}"></i>Sales Detail</a></li>
                @endif
                </ul>
            </li>
            <li class="dropdown">
                <a href="#" class="nav-link has-dropdown" data-toggle="dropdown"><i class="fas fa-table"></i> <span>Report</span></a>
                <ul class="dropdown-menu">
                <li><a class="nav-link" href="{{ url('report/outlet') }}"></i>
                    Transaction Recap Outlet</a></li>
                </ul>
              </li>
              {{-- <li class="dropdown">
                <a href="#" class="nav-link has-dropdown" data-toggle="dropdown"><i class="fas fa-table"></i> <span>Stock Moving</span></a>
                <ul class="dropdown-menu">
                <li><a class="nav-link" href="{{ url('/item-transfer') }}"></i>Item Transfer</a></li>
                </ul>
              </li> --}}
    </aside>
</div>

<!-- Main Content -->
<div class="main-content">
    <section class="section">
        <div class="section-header">
            <h1>User Rules</h1>
            <div class="section-header-breadcrumb">
                <div class="breadcrumb-item active"><a href="{{ url('/') }}">Home</a></div>
                <div class="breadcrumb-item">User Management</div>
                <div class="breadcrumb-item">User Rules</div>
            </div>
        </div>

        <div class="section-body">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="container-fluid">
                                <a onclick="addForm()" id="addForm" class="btn btn-outline-primary float-right">
                                    <i class="fas fa-plus"></i> Add</a>
                                <a onclick="refresh()" class="btn btn-outline-warning float-right">
                                    <i class="fas fa-sync-alt"></i> Refresh</a>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-striped" id="user-role-datatable">
                                    <thead>
                                        <tr>
                                            <th>Username</th>
                                            <th>Role</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

{{-- Modal Add userRole--}}
<div class="modal fade" id="modal-form" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="post">
                    @csrf
                    @method('POST')
                    <input type="hidden" id="id" name="id">
                    <div class="form-group">
                        <label for="username" class="col-form-label">Username</label>
                            <select class="form-control" style="width:100%;" id="username" name="user_id" required>
                            </select>                       
                    </div>

                    <div class="form-group">
                        <label for="role" class="col-form-label">Role</label>
                            <select class="form-control" id="role" name="role_id" required>
                                <option disabled selected>-</option>
                                <option value="1">ADMIN</option>
                                <option value="2">SPV</option>
                                <option value="3">SPG</option>
                                <option value="4">WAREHOUSE</option>
                                <option value="5">SALES</option>
                                <option value="6">MANAGER</option>
                            </select>                       
                    </div>
             
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
{{-- /Modal Add userRole --}}

@endsection

@section('javascript')
<script type="text/javascript">
    var table = $('#user-role-datatable').DataTable({
        processing: true,
        //   serverSide: true,
        ajax: "{{ url('api/user-role') }}",
        columns: [
            {data: 'username', name: 'username'},
            {data: 'role', name: 'role'},
            {data: 'action', name: 'action', orderable: false, searchable: false }
        ]
    });
    
    function refresh() {
        table.ajax.reload();
    }


    function addForm() {
        $('input[name=_method]').val('POST');
        $('#modal-form').modal('show');
        $('#modal-form form')[0].reset();
        $.ajax({
            url: "{{ url('api/user-role/list/user') }}",
            type: "GET",
            dataType: "JSON",
            success: function(response){
                $('#username').empty();
                $('#username').append('<option disabled selected>-</option>');
                $.each(response.data, function(key, val){
                    $('#username').append('<option value="'+ val.id +'">'+ val.username +'</option>');
                }); 
              }
            });
        $('.modal-title').text('Input Data');
    }

    $(document).ready(function() {
        $('#username').select2({
            dropdownParent: $(".modal")
        });
    });

    function deleteData(id){
        var csrf_token = $('meta[name="csrf-token"]').attr('content');
        swal({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            cancelButtonColor: '#d33',
            confirmButtonColor: '#3085d6',
            confirmButtonText: 'Yes, delete it!'
        }).then(function () {
            $.ajax({
                url : "{{ url('api/user-role') }}" + '/' + id,
                type : "POST",
                data : {'_method' : 'DELETE', '_token' : csrf_token},
                success : function(data) {
                    table.ajax.reload();
                    swal({
                        title: 'Success!',
                        text: data.response_description,
                        type: 'success',
                        timer: '1500'
                    })
                },
                error : function () {
                    swal({
                        title: 'Oops...',
                        text: data.message,
                        type: 'error',
                        timer: '1500'
                    })
                }
            });
        });
    }


    $(function () {
        $('#modal-form form').on('submit', function (e) {
            if (!e.isDefaultPrevented()) {
                var id = $('#id').val();
                $.ajax({
                    url: "{{ url('api/user-role') }}",
                    type: "POST",
                    data: $('#modal-form form').serialize(),
                    success: function (data) {
                        console.log(data);
                        
                        $('#modal-form').modal('hide');
                        table.ajax.reload();
                        swal({
                            title: 'Success!',
                            type: 'success',
                            timer: '2000'
                        })
                    },
                    error: function (data) {
                        swal({
                            title: 'Opps...',
                            type: 'error',
                            text: data.responseJSON.message,
                            timer: '2000'
                        })
                    }
                });
                return false;
            }
        });
    });

</script>

@endsection
