@extends('layouts.master')

@section('content')
<div class="main-sidebar">
    <aside id="sidebar-wrapper">
        <div class="sidebar-brand">
            <a href="{{ url('/') }}">Consignment</a>
        </div>
        <div class="sidebar-brand sidebar-brand-sm">
            <a href="{{ url('/') }}">Cons</a>
        </div>
        <ul class="sidebar-menu">
            <li class="menu-header">Main Navigation</li>
            <li class="dropdown">
                <a href="{{ url('/') }}" class="nav-link"><i class="fas fa-fire"></i><span>Dashboard</span></a>
            </li>
            @if(Session::get('role') == 'ADMIN')
            <li class="dropdown active">
                <a href="#" class="nav-link has-dropdown" data-toggle="dropdown"><i class="far fa-user"></i> 
                    <span>User Management</span></a>
                <ul class="dropdown-menu">
                    <li class="active"><a class="nav-link" href="{{ url('/user') }}"></i>User</a></li>
                    <li><a class="nav-link" href="{{ url('/user-role') }}"></i>User Rules</a></li>
                    <li><a class="nav-link" href="{{ url('/user-outlet') }}"></i>Mapping User - Outlet</a></li>
                </ul>
            </li>
            <li class="dropdown">
                <a href="{{ url('/article') }}" class="nav-link"><i class="fas fa-table"></i><span>Article</span></a>
            </li>
            <li class="dropdown">
                <a href="{{ url('/outlet') }}" class="nav-link"><i class="fas fa-table"></i><span>Outlet</span></a>
            </li>
            <li class="dropdown">
                <a href="{{ url('/event') }}" class="nav-link"><i class="fas fa-table"></i><span>Event</span></a>
            </li>
            @endif
            <li class="dropdown">
                <a href="#" class="nav-link has-dropdown" data-toggle="dropdown"><i class="fas fa-table"></i> <span>Sales</span></a>
                <ul class="dropdown-menu">
                <li><a class="nav-link" href="{{ url('/sales') }}"></i>List Sales</a></li>
                @if(Session::get('role') == 'ADMIN')
                <li><a class="nav-link" href="{{ url('/edit-sku') }}"></i>Edit Sku Event</a></li>
                <li><a class="nav-link" href="{{ url('/sales-detail') }}"></i>Sales Detail</a></li>
                @endif
                </ul>
            </li>
            <li class="dropdown">
                <a href="#" class="nav-link has-dropdown" data-toggle="dropdown"><i class="fas fa-table"></i> <span>Report</span></a>
                <ul class="dropdown-menu">
                <li><a class="nav-link" href="{{ url('report/outlet') }}"></i>
                    Transaction Recap Outlet</a></li>
                </ul>
              </li>
              {{-- <li class="dropdown">
                <a href="#" class="nav-link has-dropdown" data-toggle="dropdown"><i class="fas fa-table"></i> <span>Stock Moving</span></a>
                <ul class="dropdown-menu">
                <li><a class="nav-link" href="{{ url('/item-transfer') }}"></i>Item Transfer</a></li>
                </ul>
              </li> --}}
    </aside>
</div>

<!-- Main Content -->
<div class="main-content">
    <section class="section">
        <div class="section-header">
            <h1>User</h1>
            <div class="section-header-breadcrumb">
                <div class="breadcrumb-item active"><a href="{{ url('/') }}">Home</a></div>
                <div class="breadcrumb-item">User Management</div>
                <div class="breadcrumb-item">User</div>
            </div>
        </div>

        <div class="section-body">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="col-md-2 col-sm">
                                <a href="#" id="export_excel" class="btn btn-info">Export Excel <i class="far fa-file-excel"></i></a>
                            </div>
                            <div class="container-fluid">
                                <a onclick="addFormUsers()" class="btn btn-outline-primary float-right">
                                    <i class="fas fa-plus"></i> Add</a>
                                <a onclick="refreshTableUser()" class="btn btn-outline-warning float-right">
                                    <i class="fas fa-sync-alt"></i> Refresh</a>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-striped" id="users-datatable">
                                    <thead>
                                        <tr>          
                                            <th>UserID</th>                                                '
                                            <th>Username</th>
                                            <th>Nama</th>
                                            <th>Alamat</th>
                                            <th>No. Tlp</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

{{-- Modal User--}}
<div class="modal fade" id="modal-form" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="post">
                    @csrf
                    @method('POST')
                    <input type="hidden" id="status" name="status" value="1">
                    <div class="form-group" id="userid">
                        <label for="id" class="col-form-label">UserID</label>
                        <input type="text" id="id" name="id" class="form-control" readonly>                        
                    </div>
                    <div class="form-group">
                        <label for="fullname" class="col-form-label">Nama</label>
                        <input type="text" name="fullname" class="form-control" id="fullname" minlength=5 required>
                    </div>
                    <div class="form-group">
                        <label for="username" class="col-form-label">Username</label>
                        <input type="text" name="username" class="form-control" id="username" minlength=5 required>
                    </div>
                    <div class="form-group" id="form-password">
                        <label for="password" class="col-form-label">Password</label>
                        <input type="password" name="password" class="form-control" id="password" minlength=3>
                    </div>
                    <div class="form-group">
                        <label for="address" class="col-form-label">Alamat</label>
                        <textarea name="address" class="form-control" id="address"></textarea>   
                    </div>
                    <div class="form-group">
                        <label for="phone" class="col-form-label">No. Tlp</label>
                        <input type="text" name="phone" class="form-control" id="phone" required>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
{{-- /Modal User --}}


{{-- Modal Form Change Password --}}
<div class="modal fade" id="modal-form-change-password" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Change Password</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="post">
                    @csrf
                    @method('PUT')
                    <input type="hidden" id="user_id" name="user_id">
                    <div class="form-group">
                        <label for="username_read_only" class="col-form-label">Username</label>
                        <input type="text" id="username_read_only" name="username_read_only" class="form-control" minlength=5 readonly>
                    </div>
                    <div class="form-group">
                        <label for="change_password" class="col-form-label">Password</label>
                        <input type="text" id="change_password" name="change_password" class="form-control" minlength=3 required>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
{{-- /Modal Form Change Password --}}

@endsection

@section('javascript')

<script type="text/javascript">
    var table = $('#users-datatable').DataTable({
        processing: true,
        //   serverSide: true,
        ajax: "{{ url('api/user') }}",
        columns: [
            {data: 'id', name: 'id'},
            {data: 'username', name: 'username'},
            {data: 'fullname', name: 'fullname'},
            {data: 'address', name: 'address'},
            {data: 'phone', name: 'phone'},
            {data: 'action', name: 'action', orderable: false, searchable: false }
        ]
    });

    function refreshTableUser() {
        table.ajax.reload();
    }

    //export excel
    $('#export_excel').click(function(){        
        window.open("{{url('/')}}"+"/api/user/report/xls");
    });

    function changePassword(id) {
        save_method = "changepassword";
        $('#modal-form-change-password form')[0].reset();
        $.ajax({
            url: "{{ url('api/user') }}" + '/' + id,
            type: "GET",
            dataType: "JSON",
            success: function (data) {
                $('#modal-form-change-password').modal('show');
                $('.modal-title').text('Change Password');

                $('#user_id').val(data.id);
                $('#username_read_only').val(data.username);
            },
            error: function () {
                alert("Nothing Data");
            }
        });
    }

    $(function(){
        $('#modal-form-change-password form').on('submit', function (e) {
            if (!e.isDefaultPrevented()){
                $.ajax({
                    url : "{{ route('password.change') }}",
                    type : "PUT",
                    data : $('#modal-form-change-password form').serialize(),
                    success : function(data) {
                        table.ajax.reload();
                        swal({
                            title: 'Success!',
                            text: data.message,
                            type: 'success',
                            timer: '1500'
                        })
                        $('#modal-form-change-password').modal('hide');
                    },
                    error : function(data){
                        swal({
                            title: 'Opps...',
                            text: data.responseJSON.message,
                            type: 'error',
                            timer: '2000'
                        })
                    }
                });
                return false;
            }
        });
    });

    function addFormUsers() {
        save_method = "add";
        $('input[name=_method]').val('POST');
        $('#form-password').show();
        $('#userid').hide();
        $('#modal-form').modal('show');
        $('#modal-form form')[0].reset();
        $('.modal-title').text('Input Data');
    }

    function editFormUser(id) {
        save_method = "edit";
        $('input[name=_method]').val('PATCH');
        $('#form-password').hide();
        $('#modal-form form')[0].reset();
        $.ajax({
            url: "{{ url('api/user') }}" + '/' + id,
            type: "GET",
            dataType: "JSON",
            success: function (data) {

                $('#modal-form').modal('show');
                $('#userid').show();
                $('.modal-title').text('Edit Data');

                $('#id').val(data.id);
                $('#fullname').val(data.fullname);
                $('#username').val(data.username);
                $('#address').val(data.address);
                $('#phone').val(data.phone);
            },
            error: function () {
                alert("Nothing Data");
            }
        });
    }

    function deleteUser(id){
        var csrf_token = $('meta[name="csrf-token"]').attr('content');
        swal({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            cancelButtonColor: '#d33',
            confirmButtonColor: '#3085d6',
            confirmButtonText: 'Yes, delete it!'
        }).then(function () {
            $.ajax({
                url : "{{ url('api/user') }}" + '/' + id,
                type : "POST",
                data : {'_method' : 'DELETE', '_token' : csrf_token},
                success : function(response) {
                    table.ajax.reload();
                    swal({
                        title: 'Success!',
                        text: response.response_description,
                        type: 'success',
                        timer: '1500'
                    })
                },
                error : function (response) {
                    swal({
                        title: 'Oops...',
                        text: response.responseJSON.response_description,
                        type: 'error',
                        timer: '1500'
                    })
                }
            });
        });
    }


    $(function(){
        $('#modal-form form').on('submit', function (e) {
            if (!e.isDefaultPrevented()){
                var id = $('#id').val();
                if (save_method == 'add') url = "{{ url('api/user') }}";
                else url = "{{ url('api/user') . '/' }}" + id;

                $.ajax({
                    url : url,
                    type : "POST",
                    data : $('#modal-form form').serialize(),
                    success : function(data) {
                        table.ajax.reload();
                        swal({
                            title: 'Success!',
                            text: data.message,
                            type: 'success',
                            timer: '1500'
                        })
                      $('#modal-form').modal('hide');
                    },
                    error : function(data){
                        swal({
                            title: 'Opps...',
                            text: data.responseJSON.response_description,
                            type: 'error',
                            timer: '2000'
                        })
                    }
                });
                return false;
            }
        });
    });

</script>

@endsection
